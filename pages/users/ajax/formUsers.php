<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = $_POST['value'];
$id = $_POST['id'];
$arr_role_list = array();

$branch_cdoe = "";

if($action == 'EDIT'){
  $btn = 'Update changes';

  $sql   = "SELECT * FROM t_user WHERE user_id = '$id'";

  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $errorInfo  = $json['errorInfo'];
  $dataCount  = $json['dataCount'];
  $row        = $json['data'];

  //echo $sql;

  $user_login   = $row[0]['user_login'];
  $user_name    = $row[0]['user_name'];
  $user_last    = $row[0]['user_last'];
  $user_email   = $row[0]['user_email'];
  $role_list    = $row[0]['role_list'];
  $user_password= $row[0]['user_password'];
  $note1        = $row[0]['note1'];
  $note2        = $row[0]['note2'];
  $is_active    = $row[0]['is_active'];
  $user_img     = isset($row[0]['user_img'])?$row[0]['user_img']:"";
  $branch_code  = $row[0]['branch_code'];
  $department_id = $row[0]['department_id'];
  if(!empty($role_list)){
      $arr_role_list = explode(",",$role_list);
  }

}
if($action == 'ADD'){
 $btn = 'Save changes';
}
?>
<input type="hidden" id="action" name="action" value="<?=$action?>">
<input type="hidden" name="user_id" value="<?=@$id?>">
<div class="modal-body">
  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <label>User Login</label>
        <input value="<?=@$user_login?>" name="user_login" type="text" class="form-control" placeholder="User Login" required>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>อีเมล์</label>
        <input value="<?=@$user_email?>" name="user_email" type="email" class="form-control" placeholder="Email" >
      </div>
    </div>
    <?php if($action == 'ADD'){ ?>
    <div class="col-md-5">
      <div class="form-group">
        <label>Password</label>
        <input value="" name="user_password" id="pass1" type="password" autocomplete="new-password" class="form-control" placeholder="Password" required>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>Confirm Password</label>
        <input value="" name="cfm_user_password" id="pass2" type="password" autocomplete="new-password" class="form-control" placeholder="Confirm Password" required>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>Gen Pass</label>
        <button type="button" onclick="CreatePass()" class="btn btn-primary btn-flat btn-block">Gen Pass</button>
      </div>
    </div>
    <?php } ?>
    <div class="col-md-6">
      <div class="form-group">
        <label>ชื่อ</label>
        <input value="<?=@$user_name?>" name="user_name" type="text" class="form-control" placeholder="Name" required>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>นามสกุล</label>
        <input value="<?=@$user_last?>" name="user_last" type="text" class="form-control" placeholder="Last" required>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>สาขา</label>
        <select name="branch_code"  id="branch_id" class="form-control select2" style="width: 100%;" required >
          <option value="" ></option>
          <?= getoptionBranch($branch_code) ?>
        </select>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>สังกัด/แผนก</label>
        <select name="department_code"  id="department_id" class="form-control select2" style="width: 100%;" required >
          <option value="" ></option>
          <?= getoptionDepartment($department_id) ?>
        </select>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>Image</label>
        <input name="user_img" type="file" class="form-control custom-file-input" accept="image/x-png,image/gif,image/jpeg">
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>
          <img src="<?= $user_img ?>" onerror="this.onerror='';this.src='images/user.png'" style="height: 60px;">
        </label>
      </div>
    </div>
    <div class="<?=$action=='ADD'?"col-md-4":"col-md-4";?>">
      <div class="form-group">
        <label>Status</label>
        <select name="is_active" class="form-control select2" style="width: 100%;" required>
          <option value="Y" <?=@$is_active=='Y'?"selected":""?>>ใช้งาน</option>
          <option value="N" <?=@$is_active=='N'?"selected":""?>>ไม่ใช้งาน</option>
        </select>
      </div>
    </div>
    <div class="col-md-12">
      <div class="form-group">
        <label>รายละเอียด</label>
        <input value="<?=@$note1?>" name="note1" type="text" class="form-control" placeholder="รายละเอียด" >
      </div>
    </div>
    <div class="col-md-12">
      <label>Select Role</label>
    </div>
    <div class="col-md-12">
      <div class="form-group">
        <?php
          $flag   = in_array('0',$arr_role_list)?false:true;
          $sqlp   = "SELECT * FROM t_role WHERE is_active ='Y' and  role_id <> 999 ORDER BY role_id ASC";

          $queryp     = DbQuery($sqlp,null);
          $json       = json_decode($queryp, true);
          $errorInfo  = $json['errorInfo'];
          $dataCount  = $json['dataCount'];
          $rowp       = $json['data'];

          foreach ($rowp as $key => $value) {
            $display = "";
            if($value['role_id'] == 0 and $flag){
              $display = "style='display:none;'";
            }
        ?>
        <div class="col-md-6" <?=$display?> >
          <div class="checkbox">
            <label>
              <input type="checkbox" name="role_list[]" value="<?=$value['role_id']?>"
              <?= @in_array($value['role_id'], @$arr_role_list)?"checked":"" ?> required>
              <?=$value['role_name']?>
            </label>
          </div>
        </div>
        <?php } ?>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;">บันทึก</button>
</div>
