
showTable();

function showTable(){
  $.get( "ajax/showTable.php")
  .done(function( data ) {
    $("#showTable").html( data );
  });
}

function showForm(value="",id=""){
  $.post("ajax/formUsers.php",{value:value,id:id})
    .done(function( data ) {
      $('#myModal').modal({backdrop:'static'});
      $('#show-form').html(data);
  });
}

function CreatePass(){
  var pass = GenPass();
  $('#pass1').val(pass);
  $('#pass2').val(pass);
}

function resetPass(id){
  $.post("ajax/formResetPass.php",{id:id})
    .done(function( data ) {
      $('#myModalReset').modal({backdrop:'static'});
      $('#show-form-rw').html(data);
  });
}



function exportExcel()
{
   postURL_blank("../../template/user_excel_template.xlsx");
}

function upload(){
  $.get("ajax/formUpload.php")
  .done(function( data ) {
      $('#myModal2').modal({backdrop:'static'});
      $("#formShow2").html(data);
  });
}

function showHistoryUpload(){
  $.get("ajax/showHisUpload.php")
  .done(function( data ) {
      $("#historyUpload").html(data);
  });
}


function del(id,name){
  $.smkConfirm({
    text:'ยืนยันการลบข้อมูล User : '+ name +' ?',
    accept:'Yes',
    cancel:'No'
  },function(res){
    // Code here
    if (res) {
      $.post("ajax/AEDUsers.php",{action:'DEL',user_id:id})
        .done(function( data ) {
          $.smkProgressBar({
            element:'body',
            status:'start',
            bgColor: '#fff',
            barColor: '#242d6d',
            content: 'Loading...'
          });
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            showTable();
            showSlidebar();
            $.smkAlert({text: data.message,type: data.status});
          }, 1000);
      });
    }
  });
}

$('#formDataUpload').on('submit', function(event) {
  event.preventDefault();
  $('#loading').removeClass("none");
  $('#loading').addClass('loading');
  if ($('#formDataUpload').smkValidate()) {
    $.ajax({
        url: 'ajax/manageUpload.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function(data) {
        showHistoryUpload();
        $('#loading').removeClass('loading');
        $('#loading').addClass('none');
        showTable();
    }).fail(function (jqXHR, exception) {
        alert("ไม่สามารถนำเข้าได้");
        $('#loading').removeClass('loading');
        $('#loading').addClass('none');
    });
  }else{
    $('#loading').removeClass('loading');
    $('#loading').addClass('none');
  }
});

$('#formResetPassword').on('submit', function(event) {
  event.preventDefault();
  var action = $('#action').val();
  if ($('#formResetPassword').smkValidate()) {
      if( $.smkEqualPass('#pass1', '#pass2') ){
          $.ajax({
              url: 'ajax/AEDUsers.php',
              type: 'POST',
              data: new FormData( this ),
              processData: false,
              contentType: false,
              dataType: 'json'
          }).done(function( data ) {
            $.smkProgressBar({
              element:'body',
              status:'start',
              bgColor: '#ecf0f5',
              barColor: '#242d6d',
              content: 'Loading...'
            });
            setTimeout(function(){
              $.smkProgressBar({status:'end'});
              $('#formResetPassword').smkClear();
              showTable();
              showSlidebar();
              $.smkAlert({text: data.message,type: data.status});
              $('#myModalReset').modal('toggle');
            }, 1000);
          });
      }
  }
});

$('#formAddUsers').on('submit', function(event) {
  event.preventDefault();
  var action = $('#action').val();
  if ($('#formAddUsers').smkValidate()) {

    if(action == 'ADD'){
        if( $.smkEqualPass('#pass1', '#pass2') ){
          // Code here
            $.ajax({
                url: 'ajax/AEDUsers.php',
                type: 'POST',
                data: new FormData( this ),
                processData: false,
                contentType: false,
                dataType: 'json'
            }).done(function( data ) {
              $.smkProgressBar({
                element:'body',
                status:'start',
                bgColor: '#ecf0f5',
                barColor: '#242d6d',
                content: 'Loading...'
              });
              setTimeout(function(){
                $.smkProgressBar({status:'end'});
                $('#formAddUsers').smkClear();
                showTable();
                showSlidebar();
                $.smkAlert({text: data.message,type: data.status});
                $('#myModal').modal('toggle');
              }, 1000);
            });
        }
    }else{
        $.ajax({
            url: 'ajax/AEDUsers.php',
            type: 'POST',
            data: new FormData( this ),
            processData: false,
            contentType: false,
            dataType: 'json'
        }).done(function( data ) {
          $.smkProgressBar({
            element:'body',
            status:'start',
            bgColor: '#ecf0f5',
            barColor: '#242d6d',
            content: 'Loading...'
          });
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            $('#formAddUsers').smkClear();
            showTable();
            showSlidebar();
            $.smkAlert({text: data.message,type: data.status});
            $('#myModal').modal('toggle');
          }, 1000);
        });
    }
  }
});
