<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>

<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th>รอบรับวางบิล</th>
      <th>วันตัดรอบใบส่งของ</th>
      <th>วันรับเช็ค<br>CR.15 วัน</th>
      <th>วันรับเช็ค<br>CR.30 วัน</th>
      <th>วันรับเช็ค<br>CR.45 วัน</th>
      <th>วันรับเช็ค<br>CR.60 วัน</th>
      <th>วันรับเช็ค<br>CR.90 วัน</th>
      <th style="width:50px;">Edit</th>
      <th style="width:50px;">Del</th>
    </tr>
  </thead>
  <tbody>
    <?php
      $sqls   = "SELECT * FROM t_bill_date ORDER BY bill_date";

      $querys     = DbQuery($sqls,null);
      $json       = json_decode($querys, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];

      for($i=0 ; $i < $dataCount ; $i++) {
    ?>
    <tr class="text-center">
      <td><?= DateThai($rows[$i]['bill_date']); ?></td>
      <td><?= DateThai($rows[$i]['waybill_date']); ?></td>
      <td><?= DateThai($rows[$i]['cr_15']); ?></td>
      <td><?= DateThai($rows[$i]['cr_30']); ?></td>
      <td><?= DateThai($rows[$i]['cr_45']); ?></td>
      <td><?= DateThai($rows[$i]['cr_60']); ?></td>
      <td><?= DateThai($rows[$i]['cr_90']); ?></td>
      <td>
        <a class="btn_point"><i class="fa fa-edit" onclick="showForm('EDIT','<?=$rows[$i]['bill_date_id']?>')"></i></a>
      </td>
      <td>
        <a class="btn_point text-red"><i class="fa fa-trash-o" onclick="del('<?=$rows[$i]['bill_date_id']?>','<?=DateThai($rows[$i]['bill_date']);?>')"></i></a>
      </td>
    </tr>
    <?php } ?>
  </tbody>
</table>
<script>
  $(function () {
    $(function () {
      $('#tableDisplay').DataTable({
        'paging'      : true,
        'lengthChange': false,
        'searching'   : true,
        'ordering'    : false,
        'info'        : true,
        'autoWidth'   : false
      })
    })
  })
</script>
