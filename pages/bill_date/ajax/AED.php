<?php

include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');

$action       = isset($_POST['action'])?$_POST['action']:"";
$bill_date_id = isset($_POST['bill_date_id'])?$_POST['bill_date_id']:"";

$user_id_update     = $_SESSION['member'][0]['user_id'];

$sql = "";
$dateTime = date('Y/m/d H:i:s');

$arr = array();
// --ADD EDIT DELETE Module-- //
if(empty($bill_date_id) && $action == 'ADD'){
  $arr['bill_date']     = dateThToEn($_POST['bill_date'],"dd/mm/yyyy","/");
  $arr['waybill_date']  = dateThToEn($_POST['waybill_date'],"dd/mm/yyyy","/");
  $arr['cr_15']         = dateThToEn($_POST['cr_15'],"dd/mm/yyyy","/");
  $arr['cr_30']         = dateThToEn($_POST['cr_30'],"dd/mm/yyyy","/");
  $arr['cr_45']         = dateThToEn($_POST['cr_45'],"dd/mm/yyyy","/");
  $arr['cr_60']         = dateThToEn($_POST['cr_60'],"dd/mm/yyyy","/");
  $arr['cr_90']         = dateThToEn($_POST['cr_90'],"dd/mm/yyyy","/");
  $sql = DBInsertPOST($arr,'t_bill_date');
}else if($action == 'EDIT'){
  $arr['bill_date_id']  = $bill_date_id;
  $arr['bill_date']     = dateThToEn($_POST['bill_date'],"dd/mm/yyyy","/");
  $arr['waybill_date']  = dateThToEn($_POST['waybill_date'],"dd/mm/yyyy","/");
  $arr['cr_15']         = dateThToEn($_POST['cr_15'],"dd/mm/yyyy","/");
  $arr['cr_30']         = dateThToEn($_POST['cr_30'],"dd/mm/yyyy","/");
  $arr['cr_45']         = dateThToEn($_POST['cr_45'],"dd/mm/yyyy","/");
  $arr['cr_60']         = dateThToEn($_POST['cr_60'],"dd/mm/yyyy","/");
  $arr['cr_90']         = dateThToEn($_POST['cr_90'],"dd/mm/yyyy","/");

  $sql = DBUpdatePOST($arr,'t_bill_date','bill_date_id');

}else if($action == 'DEL'){
  $sql = "DELETE FROM t_bill_date WHERE bill_date_id = '$bill_date_id';";
}
// --ADD EDIT Module-- //
//echo $sql;

$query      = DbQuery($sql,null);
$row        = json_decode($query, true);
$errorInfo  = $row['errorInfo'];

if(intval($row['errorInfo'][0]) == 0){
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'Success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail')));
}

?>
