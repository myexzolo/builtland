<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = $_POST['value'];
$bill_date_id = isset($_POST['bill_date_id'])?$_POST['bill_date_id']:"";
$page_icon    = 'fa fa-circle-o';
$role_access  = '';
$arr_page_list = array();


$bill_date    = "";
$waybill_date = "";
$cr_15        = "";
$cr_30        = "";
$cr_45        = "";
$cr_60        = "";
$cr_90        = "";

$backgroundColor = "background-color:#fff;";

if($action == 'EDIT'){

  $sql   = "SELECT * FROM t_bill_date WHERE bill_date_id = '$bill_date_id'";

  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $row        = $json['data'];

  $bill_date    = DateThai($row[0]['bill_date']);
  $waybill_date = DateThai($row[0]['waybill_date']);
  $cr_15        = DateThai($row[0]['cr_15']);
  $cr_30        = DateThai($row[0]['cr_30']);
  $cr_45        = DateThai($row[0]['cr_45']);
  $cr_60        = DateThai($row[0]['cr_60']);
  $cr_90        = DateThai($row[0]['cr_90']);
}

?>
<input type="hidden" name="action" value="<?=$action?>">
<input type="hidden" name="bill_date_id" value="<?=$bill_date_id?>">
<div class="modal-body">
  <div class="row">
    <div class="col-md-4">
        <div class="form-group">
          <label>รอบรับวางบิล</label>
          <div class="date">
                <input class="input-medium form-control" required value="<?= $bill_date ?>" name="bill_date" id="bill_date" type="text" data-smk-msg="&nbsp;" data-provide="datepicker" data-date-language="th-th" readonly style="<?=$backgroundColor?>">
          </div>
      </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
          <label>วันตัดรอบใบส่งของ</label>
          <div class="date">
                <input class="input-medium form-control" required value="<?= $waybill_date ?>" name="waybill_date" id="waybill_date" type="text" data-smk-msg="&nbsp;" data-provide="datepicker" data-date-language="th-th" readonly style="<?=$backgroundColor?>">
          </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-4">
        <div class="form-group">
          <label>วันรับเช็ค CR.15 วัน</label>
          <div class="date">
                <input class="input-medium form-control" required value="<?= $cr_15 ?>" name="cr_15" id="cr_15" type="text" data-smk-msg="&nbsp;" data-provide="datepicker" data-date-language="th-th" readonly style="<?=$backgroundColor?>">
          </div>
      </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
          <label>วันรับเช็ค CR.30 วัน</label>
          <div class="date">
                <input class="input-medium form-control" required value="<?= $cr_30 ?>" name="cr_30" id="cr_30" type="text" data-smk-msg="&nbsp;" data-provide="datepicker" data-date-language="th-th" readonly style="<?=$backgroundColor?>">
          </div>
      </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
          <label>วันรับเช็ค CR.45 วัน</label>
          <div class="date">
                <input class="input-medium form-control" required value="<?= $cr_45 ?>" name="cr_45" id="cr_45" type="text" data-smk-msg="&nbsp;" data-provide="datepicker" data-date-language="th-th" readonly style="<?=$backgroundColor?>">
          </div>
      </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
          <label>วันรับเช็ค CR.60 วัน</label>
          <div class="date">
                <input class="input-medium form-control" required value="<?= $cr_60 ?>" name="cr_60" id="cr_60" type="text" data-smk-msg="&nbsp;" data-provide="datepicker" data-date-language="th-th" readonly style="<?=$backgroundColor?>">
          </div>
      </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
          <label>วันรับเช็ค CR.90 วัน</label>
          <div class="date">
                <input class="input-medium form-control" required value="<?= $cr_90 ?>" name="cr_90" id="cr_90" type="text" data-smk-msg="&nbsp;" data-provide="datepicker" data-date-language="th-th" readonly style="<?=$backgroundColor?>">
          </div>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;">บันทึก</button>
</div>
