<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = $_POST['value'];
$id     = isset($_POST['id'])?$_POST['id']:"";
$page_icon    = 'fa fa-circle-o';
$role_access  = '';
$arr_page_list = array();

$companycode  = $_SESSION['branchCode'];

$EMP_CODE         = "";
$EMP_TITLE        = "";
$EMP_NAME         = "";
$EMP_LASTNAME     = "";
$EMP_POSITION     = "";
$EMP_DEPARTMENT   = "";
$EMP_SEX          = "";
$EMP_BIRTH_DATE   = "";
$EMP_TYPE         = "";
$EMP_GROUP        = "";
$EMP_NICKNAME     = "";
$EMP_USER         = "";
$EMP_PSW          = "";
$EMP_CARD_ID      = "";
$EMP_ADDRESS1     = "";
$EMP_ADDRESS2     = "";
$EMP_ADDR_SUBDISTRICT   = "";
$EMP_ADDR_DISTRICT      = "";
$EMP_ADDR_PROVINCE      = "";
$EEMP_ADDR_POSTAL       = "";
$EMP_EMAIL        = "";
$EMP_TEL          = "";
$EMP_DATE_RETRY   = "";
$EMP_PICTURE      = "";
$EMP_IS_TRAINER   = "";
$EMP_IS_STAFF     = "";
$EMP_IS_SALE     = "";
$EMP_IS_INSTRUCTOR = "";
$EMP_WAGE = "";

$branch_id = "";
$user_id   = "";

$disabled = "";

if($action == 'EDIT'){
  $btn = 'Update changes';

  $sql   = "SELECT * FROM data_mas_employee WHERE EMP_CODE = '$id' and COMPANY_CODE ='$companycode'";

  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $errorInfo  = $json['errorInfo'];
  $row        = $json['data'];
  $dataCount  = $json['dataCount'];

  $EMP_CODE         = $row[0]['EMP_CODE'];
  $EMP_TITLE        = $row[0]['EMP_TITLE'];
  $EMP_NAME         = $row[0]['EMP_NAME'];
  $EMP_LASTNAME     = $row[0]['EMP_LASTNAME'];
  $EMP_POSITION     = $row[0]['EMP_POSITION'];
  $EMP_DEPARTMENT   = $row[0]['EMP_DEPARTMENT'];
  $EMP_SEX          = $row[0]['EMP_SEX'];
  $EMP_BIRTH_DATE   = $row[0]['EMP_BIRTH_DATE'];
  $EMP_TYPE         = $row[0]['EMP_TYPE'];
  $EMP_GROUP        = $row[0]['EMP_GROUP'];
  $EMP_NICKNAME     = $row[0]['EMP_NICKNAME'];
  $EMP_DATE_INCOME  = $row[0]['EMP_DATE_INCOME'];
  $EMP_USER         = $row[0]['EMP_USER'];
  $EMP_PSW          = $row[0]['EMP_PSW'];
  $EMP_CARD_ID      = $row[0]['EMP_CARD_ID'];
  $EMP_ADDRESS1     = $row[0]['EMP_ADDRESS1'];
  $EMP_ADDRESS2     = $row[0]['EMP_ADDRESS2'];
  $EMP_ADDR_SUBDISTRICT   = $row[0]['EMP_ADDR_SUBDISTRICT'];
  $EMP_ADDR_DISTRICT      = $row[0]['EMP_ADDR_DISTRICT'];
  $EMP_ADDR_PROVINCE      = $row[0]['EMP_ADDR_PROVINCE'];
  $EMP_ADDR_POSTAL        = $row[0]['EMP_ADDR_POSTAL'];
  $EMP_EMAIL        = $row[0]['EMP_EMAIL'];
  $EMP_TEL          = $row[0]['EMP_TEL'];
  $EMP_DATE_RETRY   = $row[0]['EMP_DATE_RETRY'];
  $EMP_PICTURE      = $row[0]['EMP_PICTURE'];
  $EMP_IS_TRAINER   = $row[0]['EMP_IS_TRAINER'];
  $EMP_IS_STAFF     = $row[0]['EMP_IS_STAFF'];
  $EMP_IS_SALE      = $row[0]['EMP_IS_SALE'];
  $EMP_WAGE         = $row[0]['EMP_WAGE'];

  $EMP_IS_INSTRUCTOR = $row[0]['EMP_IS_INSTRUCTOR'];

  $sql   = "SELECT * FROM t_user WHERE user_login = '$EMP_USER' and user_id > 0 and branch_code = '$companycode' ";

  //echo  $sql;
  $queryUser      = DbQuery($sql,null);
  $jsonUser       = json_decode($queryUser, true);
  $dataCountUser  = $jsonUser['dataCount'];

  if($dataCountUser > 0){
    $rowUser      = $jsonUser['data'];

    $role_list      = $rowUser[0]['role_list'];
    $user_id        = $rowUser[0]['user_id'];
    $user_password  = $rowUser[0]['user_password'];
    $note1          = $rowUser[0]['note1'];
    $note2          = $rowUser[0]['note2'];
    $is_active      = $rowUser[0]['is_active'];
    $user_img       = isset($rowUser[0]['user_img'])?$rowUser[0]['user_img']:"";
    $branch_code    = $rowUser[0]['branch_code'];
    $department_code  = $rowUser[0]['department_code'];
    if(!empty($role_list)){
        $arr_role_list = explode(",",$role_list);
    }
    //echo ">>>>>".$user_img;
  }
}
$optionDistrict = "";
if($action == 'ADD'){
    $btn = 'Save changes';

    $sql      = "SELECT * FROM t_branch WHERE branch_code = '$companycode'";
    $query    = DbQuery($sql,null);
    $json     = json_decode($query, true);
    $row      = $json['data'];
    $code     = $row[0]['branch_id'];


    $sql = "SELECT max(EMP_CODE) as EMP_CODE FROM data_mas_employee where EMP_CODE like '$code%' and COMPANY_CODE ='$companycode' ";
    $query      = DbQuery($sql,null);
    $json       = json_decode($query, true);
    $errorInfo  = $json['errorInfo'];
    $row        = $json['data'];
    $dataCount  = $json['dataCount'];


    if($dataCount > 0){
      if($row[0]['EMP_CODE'] != null && $row[0]['EMP_CODE'] != ""){
        $lastNum  = $row[0]['EMP_CODE'] + 1;
        $EMP_CODE = sprintf("%04d", $lastNum);
      }else{
        $EMP_CODE = $code."0001";
      }
    }else{
      $EMP_CODE = $code."0001";
    }
}

$optionEmpposition    = getoptionDataMaster('EMP_POSITION',$EMP_POSITION);
$optionEmpDepartment  = getoptionDataMaster('EMP_DEPARTMENT',$EMP_DEPARTMENT);
$optionTitle          = getoptionDataMaster('PRENAME',$EMP_TITLE);
$optionEmpType        = getoptionDataMaster('EMP_TYPE',$EMP_TYPE);
$optionProvince       = getoptionProvince($EMP_ADDR_PROVINCE);
?>
<input type="hidden" name="action" value="<?=$action?>">
<input type="hidden" name="department_id" value="<?=$id?>">
<input type="hidden" id="EMP_ADDR_DISTRICT_NAME" value="<?=$EMP_ADDR_DISTRICT?>">
<input type="hidden" id="EMP_ADDR_SUBDISTRICT_NAME" value="<?=$EMP_ADDR_SUBDISTRICT?>">
<input type="hidden" name="user_id" value="<?=$user_id?>">
<div class="modal-body">
  <div class="row">
    <div class="col-md-4">
      <div class="form-group">
        <label>Employee Code</label>
        <input value="<?= $EMP_CODE ?>" name="EMP_CODE" type="text" maxlength="15" class="form-control text-uppercase" placeholder="Code" required readonly>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>ตำแหน่ง</label>
        <select name="EMP_POSITION" id="EMP_POSITION" class="form-control select2" style="width: 100%;" <?=$disabled; ?> required>
          <option value="" selected></option>
          <?= $optionEmpposition; ?>
        </select>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>สังกัด/แผนก</label>
        <select name="EMP_DEPARTMENT" id="EMP_DEPARTMENT" class="form-control select2" style="width: 100%;" <?=$disabled; ?> required>
          <option value="" selected></option>
          <?= $optionEmpDepartment; ?>
        </select>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>คำนำหน้า</label>
        <select name="EMP_TITLE" id="EMP_TITLE" class="form-control select2" style="width: 100%;" <?=$disabled; ?> required>
          <option value="" selected></option>
          <?= $optionTitle; ?>
        </select>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>ชื่อ</label>
          <input value="<?= $EMP_NAME ?>" name="EMP_NAME" type="text" class="form-control text-uppercase" placeholder="Name" required>
      </div>
    </div>
    <div class="col-md-5">
      <div class="form-group">
        <label>สกุล</label>
          <input value="<?= $EMP_LASTNAME ?>" name="EMP_LASTNAME" type="text" class="form-control text-uppercase" placeholder="LastName" required>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ชื่อเล่น</label>
          <input value="<?= $EMP_NICKNAME ?>" name="EMP_NICKNAME" type="text" class="form-control text-uppercase" placeholder="NickName" required>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>เพศ</label>
        <div class="form-control" style="border-style: hidden;">
          <input type="radio" name="EMP_SEX" id="gender_m" class="minimal" value="ชาย" <?= $EMP_SEX == 'ชาย'? 'checked':''?> required><label for="gender_m">&nbsp;ชาย</label>&nbsp;&nbsp;
          <input type="radio" name="EMP_SEX" id="gender_f" class="minimal" value="หญิง" <?= $EMP_SEX == 'หญิง'? 'checked':''?> required><label for="gender_f">&nbsp;หญิง</label>
        </div>
      </div>
    </div>
    <div class="col-md-5">
      <div class="form-group">
        <label>เลขที่บัตรประชาชน</label>
        <input value="<?= $EMP_CARD_ID ?>" name="EMP_CARD_ID" type="text" class="form-control" placeholder="ID Card" >
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>วันเดือนปีเกิด</label>
        <input value="<?= $EMP_BIRTH_DATE ?>" name="EMP_BIRTH_DATE" type="date" class="form-control" placeholder="BIRTH DATE" >
      </div>
  </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ที่อยู่</label>
        <input value="<?= $EMP_ADDRESS1?>" name="EMP_ADDRESS1" type="text" class="form-control" placeholder="Address .." >
      </div>
    </div>
    <div class="col-md-5">
      <div class="form-group">
        <label>ที่อยู่2</label>
        <input value="<?= $EMP_ADDRESS2 ?>" name="EMP_ADDRESS2" type="text" class="form-control" placeholder="Address .." >
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>จังหวัด</label>
        <select name="EMP_ADDR_PROVINCE" id="EMP_ADDR_PROVINCE" class="form-control select2" style="width: 100%;" <?=$disabled; ?> required onchange="changeProvince();">
          <option value="" selected></option>
          <?= $optionProvince; ?>
        </select>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>อำเภอ/เขต</label>
        <div id="optionDistrict" >
        <select name="EMP_ADDR_DISTRICT" id="EMP_ADDR_DISTRICT" class="form-control select2" style="width: 100%;"  onchange="changeDistrict();">
          <option value="" selected></option>
        </select>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>ตำบล/แขวง</label>
        <div id="optionSubDistrict" >
        <select name="EMP_ADDR_SUBDISTRICT" id="EMP_ADDR_SUBDISTRICT" class="form-control select2" style="width: 100%;" onchange="changeProvince();" required>
          <option value="" selected></option>
        </select>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>รหัสไปรษณีย์</label>
        <input value="<?= $EMP_ADDR_POSTAL ?>" name="EMP_ADDR_POSTAL" id="EMP_ADDR_POSTAL" type="text" class="form-control" placeholder="Zip Code" >
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>วันที่รับทำงาน</label>
        <input value="<?= $EMP_DATE_INCOME ?>" name="EMP_DATE_INCOME" type="date" class="form-control" placeholder="วันที่รับทำงาน" >
      </div>
  </div>
  <div class="col-md-5">
    <div class="form-group">
      <label>User Login</label>
      <input value="<?=$EMP_USER?>" name="EMP_USER" autocomplete="off" type="text" class="form-control" placeholder="User Login" required>
    </div>
  </div>
  <?php if($action == 'ADD'){ ?>
  <div class="col-md-5">
    <div class="form-group">
      <label>Password</label>
      <input value="" name="EMP_PSW" id="pass1" type="password" autocomplete="new-password" class="form-control" placeholder="Password" required>
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label>Confirm Password</label>
      <input value="" id="pass2" type="password" class="form-control" placeholder="Confirm Password" required>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>Gen Pass</label>
      <button type="button" onclick="CreatePass()" class="btn btn-primary btn-flat btn-block">Gen Pass</button>
    </div>
  </div>
  <?php } ?>
  <div class="col-md-4">
    <div class="form-group">
      <label>ประเภทพนักงาน</label>
      <select name="EMP_TYPE" id="EMP_TYPE" class="form-control select2" style="width: 100%;" <?=$disabled; ?> required>
        <option value="" selected></option>
        <?= $optionEmpType; ?>
      </select>
    </div>
  </div>
  <div class="col-md-8">
    <div class="form-group">
      <label>อีเมล์</label>
      <input value="<?=$EMP_EMAIL?>" name="EMP_EMAIL" type="email" class="form-control" placeholder="Email" >
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>เบอร์โทร</label>
      <input value="<?=$EMP_TEL?>" name="EMP_TEL" type="tel" class="form-control" placeholder="Tel" >
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>ค่าจ้าง Class/ครั้ง</label>
      <input value="<?=$EMP_WAGE?>" name="EMP_WAGE" type="number" class="form-control" placeholder=""  style="text-align:right;" >
    </div>
  </div>
  <div class="col-md-9">
    <div class="form-group">
      <label>สิทธิพนักงาน</label>
      <div class="checkbox">
        <label><input type="checkbox" name="EMP_IS_TRAINER" <?= $EMP_IS_TRAINER == 'Y'? 'checked':''?> value="Y">Trainer</label>&nbsp;&nbsp;
        <label><input type="checkbox" name="EMP_IS_INSTRUCTOR"  <?= $EMP_IS_INSTRUCTOR == 'Y'? 'checked':''?> value="Y">Instructor</label>&nbsp;&nbsp;
        <label><input type="checkbox" name="EMP_IS_SALE"  <?= $EMP_IS_SALE == 'Y'? 'checked':''?> value="Y">Sale</label>&nbsp;&nbsp;
        <label><input type="checkbox" name="EMP_IS_STAFF"  <?= $EMP_IS_STAFF == 'Y'? 'checked':''?> value="Y">Manager</label>&nbsp;&nbsp;
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>Image</label>
      <input name="user_img" type="file" class="form-control" >
    </div>
  </div>
  <div class="col-md-2">
    <div class="form-group">
      <label>
        <img src="<?= $user_img ?>" onerror="this.onerror='';this.src='images/user.png'" style="height: 60px;">
      </label>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
  <button type="submit" class="btn btn-primary btn-flat"><?=$btn?></button>
</div>
