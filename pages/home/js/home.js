
$(function () {
  // $('#dateRageShow').hide();
  // $('.connectedSortable').sortable({
  //   containment         : $('section.content'),
  //   placeholder         : 'sort-highlight',
  //   connectWith         : '.connectedSortable',
  //   handle              : '.box-header, .nav-tabs',
  //   forcePlaceholderSize: true,
  //   zIndex              : 999999
  // });
  // $('.connectedSortable .box-header, .connectedSortable .nav-tabs-custom').css('cursor', 'move');
  //
  // // jQuery UI sortable for the todo list
  // $('.todo-list').sortable({
  //   placeholder         : 'sort-highlight',
  //   handle              : '.handle',
  //   forcePlaceholderSize: true,
  //   zIndex              : 999999
  // });
  //
  var role          = $('#role').val();
  if(role != null && role == "finance")
  {
    checkApprove();

    setInterval(function(){
      checkApprove();
    }, 3000);
  }



  showProfile();
  $('#daterage').daterangepicker(
    {
      locale: {
        format: 'DD/MM/YYYY',
        daysOfWeek: [
           "อา",
           "จ",
           "อ",
           "พ",
           "พฤ",
           "ศ",
           "ส"
       ],
       monthNames: [
           "มกราคม",
           "กุมภาพันธ์",
           "มีนาคม",
           "เมษายน",
           "พฤษภาคม",
           "มิถุนายน",
           "กรกฎาคม",
           "สิงหาคม",
           "กันยายน",
           "ตุลาคม",
           "พฤศจิกายน",
           "ธันวาคม"
       ]
      }
    }
  );
  getBillList();

  $("#tableBill").scroll(function() {
      //console.log(">>>"+$("#tableBill").height());
  });
});


// var gdpData = {
//   "TH-57": 200,
//   "TH-56": 100,
//   "TH-55": 50,
//   "TH-54": 200
// };
var searchBill = false;
function reset()
{
  searchBill = false;
  var role          = $('#role').val();

  if(role != null && role == "finance")
  {
    $('#vendor_code').val("").trigger('change');
  }
  $("#type").val("");
  $('#daterage').val($("#daterage_tmp").val());
  searchBill = true;
  getBillList();
}

function getApprove()
{
  var vendor_code   = $('#vendor_code').val();
  var role          = $('#role').val();
  //console.log(role);
  if(role != null && role == "finance")
  {
    if(vendor_code != ""){
        vendor_code  = "";
        searchBill = false;
        $('#vendor_code').val("").trigger('change');
    }
  }

  $.post("ajax/getApprove.php",{status:'W',vendor_code:vendor_code})
    .done(function( data ) {
      //console.log(data);
      searchBill = false;
      $("#type").val('W');
      if(data.startDate != ""){
        searchBill = true;
        $('#daterage').daterangepicker({ startDate: data.startDate , endDate: data.endDate });
      }else{
        searchBill = true;
      }
  });
}

function changeStatus(status,id)
{
  if(status == 'A')
  {
    $.post("ajax/changeStatus.php",{status:status,id:id})
      .done(function( data ) {
        $.smkAlert({text: data.message,type: data.status});
        getBillList();
    });
  }else if(status == 'N'){
    $.post("ajax/formStatus.php",{status:status,id:id})
      .done(function( data ) {
        $('#myModalStatus').modal({backdrop:'static'});
        $('#myModalLabelAp').html("ยืนยันการ ไม่อนุมัติ ใบวางบิล");
        $('#show-status').html(data);
    });
  }else if(status == 'C'){
    $.post("ajax/formStatus.php",{status:status,id:id})
      .done(function( data ) {
        $('#myModalStatus').modal({backdrop:'static'});
        $('#myModalLabelAp').html("ยืนยันการ ยกเลิก ใบวางบิล");
        $('#show-status').html(data);
    });
  }else if(status == 'R'){
    $.post("ajax/formStatus.php",{status:status,id:id})
      .done(function( data ) {
        $('#myModalStatus').modal({backdrop:'static'});
        $('#myModalLabelAp').html("ยืนยันการ รีเซ็ต ใบวางบิล");
        $('#show-status').html(data);
    });
  }
}

function printDoc(id,doc_no)
{
    postURLBlank('report/index.php?id='+id+'&&doc_no='+doc_no, false);
}

function checkApprove()
{
  $( document ).ajaxStart(function() {
    $(".loadingImg").addClass('none');
  });
  $.get("ajax/checkApprove.php")
     .done(function( data ) {
       $("#checkApprove").html(data.count);
   });
}

function showForm(value,id)
{
  $.post("ajax/formBill.php",{value:value,bill_id:id})
    .done(function( data ) {
      $('#myModalBill').modal({backdrop:'static'});
      $('#show-form').html(data);
      cloneTr = $('#tablePo tbody tr:last').clone();
      cal();
      crDate();
      $('#datepicker').datepicker({autoclose: true});
  });

}

function crDate()
{
  var waybillDate   = $('#datepicker').val();
  var billDate      = $('#bill_date').val();
  var waybill_date	= dateThToEn(waybillDate,"dd/mm/yyyy","/");
  var bill_date	    = dateThToEn(billDate,"dd/mm/yyyy","/");
  var term = $('#term').val();
  var dueDate  = $('#due_date').val();

  //console.log(waybill_date+","+term+","+bill_date);
  if(term == 0){
    $('#due_date').val(waybillDate);
  }else{
    $.post("ajax/crDate.php",{waybill_date:waybill_date,term:term,bill_date:bill_date})
      .done(function( data ) {
        console.log(data.dateCr);
        $('#due_date').val(data.dateCr);
    });
  }
}



var cloneTr;
function createBill(id)
{
  $.post("ajax/formBill.php",{id:id,value:'ADD'})
    .done(function( data ) {
      $('#myModalBill').modal({backdrop:'static'});
      $('#show-form').html(data);
      cloneTr = $('#tablePo tbody tr:last').clone();
      crDate();
  });
}


function printBill(id,doc_no)
{
  var type = $("#type").val();
  var daterage    = $('#daterage').val();
  var res = daterage.split("-");
  var date_start    = dateThToEn(res[0].trim(),"dd/mm/yyyy","/");
  var date_end      = dateThToEn(res[1].trim(),"dd/mm/yyyy","/");
  var vendor_code   = $('#vendor_code').val();

  var pam  = "date_start="+date_start;
      pam  += "&&date_end="+date_end;
      pam  += "&&type="+type;
      pam  += "&&vendor_code="+vendor_code;

  postURLBlank('print/index.php?'+pam, false);
}

function checknumber(inputs){
  var valid = /^\d{0,4}(\.\d{0,2})?$/.test(inputs.value),
      val = inputs.value;
  //console.log(valid+ ", " +val);
  if(!valid){
      inputs.value = val.substring(0, val.length - 1);
  }
}

var first = 0;
function getBillList()
{
    var type = $("#type").val();
    var daterage    = $('#daterage').val();
    var res = daterage.split("-");
    var date_start    = dateThToEn(res[0].trim(),"dd/mm/yyyy","/");
    var date_end      = dateThToEn(res[1].trim(),"dd/mm/yyyy","/");
    var vendor_code   = $('#vendor_code').val();
    //console.log(daterage);
    var role = $('#role').val();
    var url  = "ajax/billList.php";
    //console.log(role);
    if(role != null && role == "finance")
    {
      url  = "ajax/billList_finance.php";
    }
    $.post(url,{date_start:date_start,date_end:date_end,type:type,vendor_code:vendor_code})
      .done(function( data ) {
        if(searchBill){
          console.log("xx");
          $("#billList").html( data );
        }
        if(first == 0){
          first++;
          searchBill = true;
          getBillList();
          $('#daterage').on('change', function(e) {
              getBillList();
          });
        }

    });
}

function openDate(name)
{
  $('#'+name).datepicker('show');
}


function readURL(input,id_tmp) {

  if (input.files) {
      var filesAmount = input.files.length;
      //$('#'+values).html('');
      //console.log(input);
      //console.log(input.files);
      for (i = 0; i < filesAmount; i++) {
          var size = input.files[i].size;
          var name = input.files[i].name;
          // var reader = new FileReader();
          if(size > 2097152){
            $.smkAlert({
              text: 'file name : '+name+' เกิน 2M !!',
              type: 'danger',
              position:'top-center'
            });
            $(input).val('');
            $("#"+id_tmp).val("");
          }else{
            $("#"+id_tmp).val(name);
          }
          // reader.onload = function(event) {
          //     $($.parseHTML("<img width='200'>")).attr('src', event.target.result).appendTo('#'+values);
          // }
          // reader.readAsDataURL(input.files[i]);
        }
  }
}


function showProfile()
{
  var typeUser = $("#typeUser").val();
  var url = "";
  if(typeUser == "vnd")
  {
      url = "ajax/profileVendor.php";
  }else if(typeUser == "adm"){
      url = "ajax/profileEmp.php";
  }

 $.get(url)
    .done(function( data ) {
      $("#showProfile").html( data );
  });
}


var path_num = 1;
function addRow(){
  var rowCount = $('#tablePo tbody tr').length;
  if(rowCount < 5){
    if(rowCount > path_num){
      path_num = rowCount;
    }else{
      path_num++;
    }

    var newId = "path_"+path_num;
    cloneTr.find("input[type=text]").val("");
    cloneTr.find("input[type=file]").val("");
    cloneTr.find("input[type=number]").val("");
    cloneTr.find(".bill_detail").val("");
    cloneTr.find(".fa-trash-o").attr("onchange","delRow(this,'')");


    var tmp_file = $("#tmp_file").html();
    cloneTr.find(".file_path").html(tmp_file);

    cloneTr.find(".path").attr("id",newId);
    cloneTr.find(".path").prop('required',true);
    cloneTr.find(".custom-file-input").attr("onchange","readURL(this,'"+newId+"')");

    $('#tablePo tbody').append(cloneTr);
    var clonelast = $('#tablePo tbody tr:last').clone();
    cloneTr = clonelast;
  }
}

function delRow(obj,id){
  var rowCount = $('#tablePo tbody tr').length;
  if(rowCount > 1){
    if(id != "")
    {
      var inp = "<input type='hidden' value='"+id+"' name='id_delete[]'>";
      $('#id_delete').append(inp);
    }
    $(obj).closest('tr').remove();
  }else{
    $.smkAlert({
      text: 'ทำรายการอย่างน้อย 1 รายการ',
      type: 'warning',
      position:'top-center'
    });
    $(input).val('');
  }
  cal();
}

function cal()
{
  var sumTotal = 0;
  $( ".amount" ).each(function() {
    sumTotal += isNumeric($(this).val().replace(",", ""));
  });
  $("#total").val(addCommas(sumTotal.toFixed(2)));
}

$('#formAddBill').on('submit', function(event) {
  event.preventDefault();
  if ($('#formAddBill').smkValidate()) {
    $.ajax({
        url: 'ajax/AED.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function(data) {
        $.smkProgressBar({
          element:'body',
          status:'start',
          bgColor: '#ecf0f5',
          barColor: '#242d6d',
          content: 'Loading...'
        });
        $.smkAlert({text: data.message,type: data.status});
        $('#formAddBill').smkClear();
        $('#myModalBill').modal('toggle');

        setTimeout(function(){
          $.smkProgressBar({status:'end'});
          getBillList();
        }, 1000);

    }).fail(function (jqXHR, exception) {
        $('#formAddBill').smkClear();
        $.smkAlert({text: "ไม่สามารถบันทึกได้",type: "danger"});
        $('#myModalBill').modal('toggle');
    });
  }
});


$('#formStatus').on('submit', function(event) {
  event.preventDefault();
  if ($('#formStatus').smkValidate()) {
    $.ajax({
        url: 'ajax/changeStatus.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function(data) {
        $.smkAlert({text: data.message,type: data.status});
        $('#formStatus').smkClear();
        $('#myModalStatus').modal('toggle');
        getBillList();
    }).fail(function (jqXHR, exception) {
        $('#formStatus').smkClear();
        $.smkAlert({text: "ไม่สามารถเปลี่ยนแปลงสถานะได้",type: "danger"});
        $('#myModalStatus').modal('toggle');
    });
  }
});

function isNumeric(num){
    if(isNaN(num) || num == "")
    {
      num = 0;
    }
    return parseFloat(num);
}
