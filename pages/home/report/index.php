<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" type="image/png" href="../../image/favicon.png"/>
  </head>
  <body>
<?php
  include("../../../inc/function/connect.php");
  include("../../../inc/function/mainFunc.php");
  require_once '../../../mpdf2/vendor/autoload.php';

  $bill_id = $_POST['id'];
  $doc_no   = $_POST['doc_no'];
  $mpdf = new \Mpdf\Mpdf();

  $defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
  $fontDirs = $defaultConfig['fontDir'];

  $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
  $fontData = $defaultFontConfig['fontdata'];

  $mpdf = new \Mpdf\Mpdf([
      'fontDir' => array_merge($fontDirs, [
          '../../../font/CSChatThai',
      ]),
      'fontdata' => $fontData + [
          'chatthai' => [
              'R' => 'CSChatThai.ttf',
              //'I' => 'THSarabunNew Italic.ttf',
              //'B' => 'THSarabunNew Bold.ttf',
          ]
      ],
      'default_font' => 'chatthai'
  ]);

  ob_start();



  $sqlbl ="SELECT b.*, v.vendor_name,v.term FROM t_bill b, t_vendor v  where b.bill_id = '$bill_id' and b.vendor_code = v.vendor_code";
  //echo $sql;
  $querybl    = DbQuery($sqlbl,null);
  $jsonbl     = json_decode($querybl, true);
  $rowbl      = $jsonbl['data'];

  $receive_date = DateThai($rowbl[0]['receive_date']);
  $vendor_code  = isset($rowbl[0]['vendor_code'])?$rowbl[0]['vendor_code']:"";
  $vendor_name  = isset($rowbl[0]['vendor_name'])?$rowbl[0]['vendor_name']:"";
  $doc_no     = isset($rowbl[0]['doc_no'])?$rowbl[0]['doc_no']:"";
  $doc_date   = is_null($rowbl[0]['doc_date'])?"":DateThai($rowbl[0]['doc_date']);
  $due_date   = is_null($rowbl[0]['due_date'])?"":DateThai($rowbl[0]['due_date']);
  $note       = is_null($rowbl[0]['note'])?"":$rowbl[0]['note'];
  $total      = is_null($rowbl[0]['total'])?"":$rowbl[0]['total'];
  $bill_date  = DateThai($rowbl[0]['create_date']);
    ?>

    <div class="noheader">
      <table width="100%">
        <thead>
        <tr>
          <th colspan="2" style="width:32%;align:left;"><img class="logo" src="../../../image/bl-logo.jpg" alt="builtland" style="width:180px;"></th>
          <th style="padding-top:20px;width:36%" class="font-10">
              บริษัท บิลท์ แลนด์ จำกัด (มหาชน)
          </th>
          <th style="width:16%"></th>
          <th style="width:16%" class="text-right"><p class="font-9 text-center">1/1</p></th>
        </tr>
        <tr>
          <th style="width:16%"></th>
          <th colspan="3">
            <p class="font-9 text-center">
              96 หมู่ 4 ซ.แจ้งวัฒนะ-ปากเกร็ด 17 ต.ปากเกร็ด อ.ปากเกร็ด จ.นนทบุรี 11120<br>
              Tel : 025842013  Fax : 025842015
            </p>
          </th>
          <th style="width:16%" class="text-right font-10" style="font-weight:400;">ใบรับเอกสาร</th>
        </tr>
        <thead>
      </table>
      <br />
      <table width="100%">
        <tr>
          <td style="width:70px" class="text-left font-10">รหัสผู้ขาย</td>
          <td style="width:400px"  class="text-left font-10"><?= $vendor_code ?></td>
          <td class="text-right font-10">เลขที่ใบรับเอกสาร</td>
          <td class="text-left font-10"><?= $doc_no ?></td>
        <tr>
        <tr>
          <td></td>
          <td class="text-left font-10"><?= $vendor_name ?></td>
          <td class="text-right font-10">วันที่ใบรับเอกสาร</td>
          <td class="text-left font-10"><?= $doc_date ?></td>
        <tr>
      </table>
      <br />
      <div style="height:500px;">
      <table width="100%" style="border-collapse: collapse;">
        <tr>
          <th class="font-10 table-borders" style="width:120px">ลำดับที่</th>
          <th class="font-10 table-borders" >เลขที่ตั้งหนี้</th>
          <th class="font-10 table-borders">วันที่</th>
          <th class="font-10 table-borders" style="width:150px">จำนวน</th>
        <tr>
        <?php
          $sql ="SELECT * FROM t_bill_detail where bill_id = '$bill_id' order by bill_detail_id";
          //echo $sql;
          $querys     = DbQuery($sql,null);
          $json       = json_decode($querys, true);
          $errorInfo  = $json['errorInfo'];
          $dataCount  = $json['dataCount'];
          $rows       = $json['data'];

          if($path_file != "" && $action == "EDIT"){
            $col = "10";
          }

          for($i=0 ; $i < $dataCount ; $i++) {
            $bill_id        = $rows[$i]['bill_id'];
            $bill_detail_id = $rows[$i]['bill_detail_id'];
            $po_no          = $rows[$i]['po_no'];
            $amount         = $rows[$i]['amount'];
            $path           = $rows[$i]['path'];
         ?>
         <tr>
           <td class="font-10 text-center" ><?= $i+1 ?></td>
           <td class="font-10"><?= $po_no ?></td>
           <td class="font-10 text-center"><?= $receive_date ?></td>
           <td class="font-10 text-right" style="width:150px"><?= number_format($amount,2) ?></td>
         </tr>
         <?php
          }
         ?>

      </table>
    </div>
    <table width="100%" style="border-collapse: collapse;">
      <tr>
        <td class="font-10 table-borders">( <?= convert(number_format($total,2))?> )</td>
        <td class="font-10 table-borders text-right" style="width:100px">รวมเงินทั้งสิ้น</td>
        <td class="font-10 table-borders text-right" style="width:150px"><?= number_format($total,2)?></td>
      <tr>
    </table>
    <br>
    <p class="font-10 text-left">หมายเหตุ :</p>
    <p class="font-8 text-left" style="line-height:25px;">* การรับวางบิลจะสมบูรณ์เมื่อบริษัทได้ทำการตรวจสอบกับเอกสารและสินค้าที่จัดส่งเป็นที่เรียบร้อยแล้ว โดยวันที่นัดรับเช็คอาจมีการเปลี่ยนแปลง หากเอกสารไม่สมบูรณ์ครบถ้วน ซึ่ง supplier สามารถตรวจสอบก่อนรับเช็คได้ที่ คุณนันจิรา (แนน) ต่อ 125</p>
    <br>
    <table width="100%">
      <tr>
        <td class="font-10" style="width:50%">กำหนดรับเช็ค : เวลา 13.00 - 14.00 น.</td>
        <td class="font-10" style="width:50%">*วันนัดรับเช็ค/โอนเงิน : <b><?= $due_date?></b></td>
      <tr>
      <tr>
        <td class="font-10 text-center" style="padding-top:50px">ชื่อผู้รับเอกสาร  .............................................................</td>
        <td class="font-10 text-center" style="padding-top:50px">วันที่รับ  .........................................</td>
      <tr>
    </table>
    </div>
    <!-- <div style="page-break-after: always"></div> -->
  </body>
</html>
<?php
  $html = ob_get_contents();
  ob_end_clean();
  $stylesheet = file_get_contents('../css/home.css');
  $mpdf->SetTitle($doc_no);
  // $mpdf->showWatermarkText = true;
  $mpdf->WriteHTML($stylesheet,\Mpdf\HTMLParserMode::HEADER_CSS);
  $mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);
  // $mpdf->Output('IVRENT201711000059.pdf', \Mpdf\Output\Destination::DOWNLOAD);
  $mpdf->Output($_reserve_code.'.pdf', \Mpdf\Output\Destination::INLINE);
?>
