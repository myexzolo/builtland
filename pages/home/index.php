<!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="google-site-verification" content="HL-4q2f8iiaLDlSvEOkSSsQuQxuPpDzXxHcOvdGBc3c" />
      <title>ระบบวางบิล Online | หน้าหลัก</title>
      <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
      <?php
        include("../../inc/css-header.php");
        $_SESSION["RE_URI"] = $_SERVER["REQUEST_URI"];
      ?>
      <link rel="stylesheet" href="css/home.css">
    </head>
    <body class="hold-transition skin-purple-light sidebar-mini" onload="showProcessbar();showSlidebar();">
      <div class="wrapper">
        <?php include("../../inc/header.php"); ?>

        <?php
          include("../../inc/sidebar.php");
          include('../../inc/function/mainFunc.php');
          $role_list = $_SESSION['member'][0]['role_list'];
          $roleArr   = explode(",",$role_list);
          $noDisplayVendor = "";
          $VendorCode = "";

          $scol1 = "col-md-5";
          $scol2 = "col-md-4";
          $scol3 = "col-md-3";

        ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <div class="row">
              <div class="col-md-4">
                <h1>หน้าหลัก</h1>
              </div>
              <div class="col-md-8 ">
                <?php
                  $typeUser = "";
                  $role = "";
                  if (in_array("2", $roleArr) || in_array("1", $roleArr)) {
                    $typeUser = "adm";
                    $role     = "finance";
                    if($_SESSION['ROLE_USER']['is_approve'])
                    {
                    ?>
                      <a class="btn btn-app btn-shortcut pull-right" onclick="getApprove()" style="font-size:20px;margin: 0 0 0px 10px;">
                        <span class="badge bg-yellow" id="checkApprove">0</span>
                        <i class="fa fa-check" style="font-size:30px;"></i> รออนุมัติ
                      </a>
                    <?php
                    }
                  }
                ?>
                <?php
                  if (in_array("999", $roleArr) || in_array("0", $roleArr)) {

                    if (in_array("0", $roleArr))
                    {
                      $typeUser = "adm";
                    }
                    if (in_array("999", $roleArr))
                    {
                      $typeUser = "vnd";
                      $noDisplayVendor = "style='display:none';";
                      // $vendor = getVendorByUserId($_SESSION['member'][0]['user_id']);
                      // $venDorID = isset($vendor[0]['vendor_id'])?$vendor[0]['vendor_id']:"";
                      $VendorCode = $_SESSION['member'][0]['user_login'];
                      $scol1 = "col-md-5";
                      $scol2 = "col-md-5";
                      $scol3 = "col-md-4";
                    }

                 if($_SESSION['ROLE_USER']['is_insert'])
                {

                ?>

                    <a class="btn btn-app btn-shortcut pull-right" onclick="createBill('<?=$_SESSION['member'][0]['user_id']; ?>')" style="font-size:20px;margin: 0 0 0px 10px;">
                      <i class="fa fa-plus" style="font-size:30px;"></i>
                      เปิดใบวางบิล
                    </a>
                <?php
                }
                }

                  $optionVendor = getoptionVendorCode($VendorCode);
                ?>
              </div>
            </div>
          </section>

          <!-- Main content -->
          <section class="content">
            <?php //include("../../inc/boxes.php");
                $date = date('01/m/Y')." - ".date('t/m/Y');
            ?>
            <input type="hidden" value="<?= $typeUser?>" id="typeUser">
            <!-- Main row -->
            <div class="row">
              <!-- Left col -->
            <section class="col-lg-12 connectedSortable ui-sortable">
          <!-- Custom tabs (Charts with tabs)-->
             <div class="box box-primary" style="box-shadow: 0px 0px 2px 0px rgba(135,133,131,1);">
               <div class="box-header ui-sortable-handle with-border" style="cursor: move;">
                 <i class="fa fa-file-text-o"></i>
                 <h3 class="box-title">รายการวางบิล</h3>
                 <input type="hidden" id="role" name="role" value="<?= $role ?>">
               </div>
               <div class="row" style="margin-top:20px;">
                 <div class="col-md-5" <?= $noDisplayVendor ?>>
                   <div class="form-group" style="margin-bottom: 10px;">
                      <label for="vendor_id" class="col-sm-3 control-label labelS">Vendor</label>
                      <div class="col-sm-9">
                        <?php if($VendorCode != ""){?>
                          <input type="hidden" id="vendor_code"  value="<?= $VendorCode; ?>">
                        <?php }else{ ?>
                          <select id="vendor_code" class="form-control select2" onchange="getBillList()" style="width:100%;">
                            <option value="" >ทั้งหมด</option>
                            <?= $optionVendor ?>
                          </select>
                        <?php }
                        ?>

                      </div>
                    </div>
                 </div>
                 <div class="<?=$scol2 ?>">
                   <div class="form-group" style="margin-bottom: 10px;">
                      <label for="date_reg" class="col-sm-4 control-label labelS">วันที่ส่งของ</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" id="daterage"  value="<?= $date; ?>" style="width:100%;">
                        <input type="hidden" id="daterage_tmp"  value="<?= $date; ?>">
                      </div>
                    </div>
                 </div>
                 <div class="<?=$scol3 ?>">
                   <div class="form-group" style="margin-bottom: 10px;">
                      <label for="type" class="col-sm-4 control-label labelS">สถานะ</label>
                      <div class="col-sm-8">
                        <select id="type" class="form-control" onchange="getBillList()" style="width:100%;">
                          <option value="" selected>ทั้งหมด</option>
                          <option value="W">รออนุมัติ</option>
                          <option value="A">อนุมัติ</option>
                          <option value="N">ไม่อนุมัติ</option>
                        </select>
                      </div>
                    </div>
                 </div>
                 <div class="col-md-12" align="center" style="padding:20px;">
                   <button type="button" class="btn btn-primary btn-flat" style="width:120px;" onclick="getBillList()"><i class="fa fa-search"></i>&nbsp;&nbsp;ค้นหา</button>
                   <button type="button" class="btn btn-warning btn-flat" style="width:120px;" onclick="reset()"><i class="fa fa-close"></i>&nbsp;&nbsp;ยกเลิก</button>
                   <button type="button" class="btn btn-default btn-flat" style="width:120px;" onclick="printBill()"><i class="fa fa-print"></i>&nbsp;&nbsp;พิมพ์</button>
                </div>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                 <div id="billList" style="width: 100%;" onscroll="checkheight()"></div>
               </div>
             </div>

            <div class="box box-primary" style="box-shadow: 0px 0px 2px 0px rgba(135,133,131,1); display:none;";>
              <div class="box-header ui-sortable-handle" style="cursor: move;">
                <i class="ion ion-clipboard"></i>
                <h3 class="box-title">To Do List</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body"></div>
              <!-- /.box-body -->
              <div class="box-footer clearfix no-border">
                <button type="button" class="btn btn-default pull-right"><i class="fa fa-plus"></i> Add item</button>
              </div>
            </div>

            <div class="box box-info" style="box-shadow: 0px 0px 2px 0px rgba(135,133,131,1); display:none;";>
              <div class="box-header ui-sortable-handle" style="cursor: move;">
                <i class="fa fa-envelope"></i>

                <h3 class="box-title">Quick Email</h3>
              </div>
              <div class="box-body">

              </div>
              <div class="box-footer clearfix">
                <button type="button" class="pull-right btn btn-default" id="sendEmail">Send
                  <i class="fa fa-arrow-circle-right"></i></button>
              </div>
            </div>
        </section>
        <section class="col-lg-12 connectedSortable ui-sortable" style="display:none">
          <div class="box box-info" style="box-shadow: 0px 0px 2px 0px rgba(135,133,131,1)";>
            <div class="box-header ui-sortable-handle" style="cursor: move;">
              <i class="fa fa-calendar"></i>
              <h3 class="box-title"></h3>Due Date
            </div>
            <div class="box-body">
              <div id="calendar"></div>
            </div>
          </div>
        </section>

        <section class="col-lg-6 connectedSortable ui-sortable" style="display:none">
          <div class="box box-info" style="box-shadow: 0px 0px 2px 0px rgba(135,133,131,1)";>
            <div class="box-header ui-sortable-handle" style="cursor: move;">
              <i class="fa fa-user"></i>
              <h3 class="box-title"></h3>สรุปวางบิลรวม
            </div>
            <div class="box-body">
              <div class="chart">
                  <canvas id="stackedBarChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                </div>
            </div>
          </div>
        </section>
          </div>
              <!-- /.row -->
        </section>
        <div class="modal fade" id="myModalBill" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">เปิดใบวางบิล</h4>
              </div>
              <form id="formAddBill" novalidate enctype="multipart/form-data">
              <!-- <form data-smk-icon="glyphicon-remove-sign" novalidate enctype="multipart/form-data" action="ajax/AED.php" method="post"> -->
                <div id="show-form"></div>
              </form>
            </div>
          </div>
        </div>


        <div class="modal fade" id="myModalStatus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabelAp">ไม่อนุมัติ</h4>
              </div>
              <form id="formStatus" novalidate enctype="multipart/form-data">
              <!-- <form data-smk-icon="glyphicon-remove-sign" novalidate enctype="multipart/form-data" action="ajax/AED.php" method="post"> -->
                <div id="show-status"></div>
              </form>
            </div>
          </div>
        </div>
          <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <?php include("../../inc/footer.php"); ?>
      </div>
      <!-- ./wrapper -->
      <?php include("../../inc/js-footer.php"); ?>
      <script src="js/home.js?v=1"></script>
      <script>
      $(function() {
          var currentYear = new Date().getFullYear();

          var redDateTime = new Date(currentYear, 2, 13).getTime();
          var circleDateTime = new Date(currentYear, 1, 20).getTime();
          var borderDateTime = new Date(currentYear, 0, 12).getTime();

          // $('#calendar').calendar({
          //     customDayRenderer: function(element, date) {
          //         if(date.getTime() == redDateTime) {
          //             $(element).css('font-weight', 'bold');
          //             $(element).css('font-size', '15px');
          //             $(element).css('color', 'green');
          //         }
          //         else if(date.getTime() == circleDateTime) {
          //             $(element).css('border-button', 'green');
          //             $(element).css('color', 'white');
          //             $(element).css('border-radius', '15px');
          //         }
          //         else if(date.getTime() == borderDateTime) {
          //             $(element).css('border', '2px solid blue');
          //         }
          //     },
          //     language : 'th'
          // });


            $(".select2").select2();

            //---------------------
           //- STACKED BAR CHART -
           //---------------------
           // var areaChartData = {
           //    labels  : ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'July', 'July'],
           //    datasets: [
           //      {
           //        label               : 'Digital Goods',
           //        backgroundColor     : 'rgba(60,141,188,0.9)',
           //        borderColor         : 'rgba(60,141,188,0.8)',
           //        pointRadius          : false,
           //        pointColor          : '#3b8bba',
           //        pointStrokeColor    : 'rgba(60,141,188,1)',
           //        pointHighlightFill  : '#fff',
           //        pointHighlightStroke: 'rgba(60,141,188,1)',
           //        data                : [28, 48, 40, 19, 86, 27, 90, 90, 90]
           //      },
           //      {
           //        label               : 'Electronics',
           //        backgroundColor     : 'rgba(210, 214, 222, 1)',
           //        borderColor         : 'rgba(210, 214, 222, 1)',
           //        pointRadius         : false,
           //        pointColor          : 'rgba(210, 214, 222, 1)',
           //        pointStrokeColor    : '#c1c7d1',
           //        pointHighlightFill  : '#fff',
           //        pointHighlightStroke: 'rgba(220,220,220,1)',
           //        data                : [65, 59, 80, 81, 56, 55, 40, 90, 90]
           //      },
           //    ]
           //  }
           // var barChartData = jQuery.extend(true, {}, areaChartData)
           //
           // var stackedBarChartCanvas = $('#stackedBarChart').get(0).getContext('2d')
           // var stackedBarChartData = jQuery.extend(true, {}, barChartData)
           //
           // var stackedBarChartOptions = {
           //   responsive              : true,
           //   maintainAspectRatio     : false,
           //   scales: {
           //     xAxes: [{
           //       stacked: true,
           //     }],
           //     yAxes: [{
           //       stacked: true
           //     }]
           //   }
           // }
           //
           // var stackedBarChart = new Chart(stackedBarChartCanvas, {
           //   type: 'bar',
           //   data: stackedBarChartData,
           //   options: stackedBarChartOptions
           // });
      });
      </script>
    </body>
  </html>
