<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" type="image/png" href="../../image/favicon.png"/>
  </head>
  <body>
<?php
  include("../../../inc/function/connect.php");
  include("../../../inc/function/mainFunc.php");
  require_once '../../../mpdf2/vendor/autoload.php';

  $con = "";

  $date_start   = isset($_POST['date_start'])?$_POST['date_start']:"";
  $date_end     = isset($_POST['date_end'])?$_POST['date_end']:"";
  $type         = isset($_POST['type'])?$_POST['type']:"";
  $vendor_code  = isset($_POST['vendor_code'])?$_POST['vendor_code']:"";
  $role         = isset($_POST['role'])?$_POST['role']:"";


  $display = "";
  if ($role != "finance") {
    $display = "style='display:none'";
  }

  $mpdf = new \Mpdf\Mpdf();

  $defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
  $fontDirs = $defaultConfig['fontDir'];

  $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
  $fontData = $defaultFontConfig['fontdata'];

  $mpdf = new \Mpdf\Mpdf([
      'fontDir' => array_merge($fontDirs, [
          '../../../font/CSChatThai',
      ]),
      'fontdata' => $fontData + [
          'chatthai' => [
              'R' => 'CSChatThai.ttf',
              //'I' => 'THSarabunNew Italic.ttf',
              //'B' => 'THSarabunNew Bold.ttf',
          ]
      ],
      'default_font' => 'chatthai'
  ]);

  ob_start();

    ?>
    <div class="noheader">
      <span class="font-9">Billing Note Report</span>
      <p class="font-10 text-center" style="margin-bottom: 0px;">
        <b>บริษัท บิลท์ แลนด์ จำกัด (มหาชน)</b>
      </p>
      <table width="100%" style="border-collapse: collapse;">
      <thead>
        <tr colspan="6"><th>&nbsp;</th></tr>
        <tr>
        <th class="font-9 table-bordered" style="width:40px">No.</th>
        <th class="font-9 table-bordered" style="width:100px">Document No.</th>
        <th class="font-9 table-bordered" style="width:130px">Doc.Receive Date</th>
        <th class="font-9 table-bordered" style="width:90px">Amount</th>
        <th class="font-9 table-bordered">Name</th>
        <th class="font-9 table-bordered" style="width:80px">Due Date</th>
        </tr>
      </thead>
      <tbody>
        <?php

        $con = "";

        if($date_start != "")
        {
          $con .= " and b.receive_date between '$date_start' and '$date_end' ";
        }

        if($type != "")
        {
          $con .= " and b.status = '$type' ";
        }

        if($vendor_code != "")
        {
          $con .= " and b.vendor_code = '$vendor_code' ";
        }

        $sql ="SELECT b.*, v.vendor_name FROM t_bill b,t_vendor v where b.vendor_code = v.vendor_code $con order by doc_no";

        $querys     = DbQuery($sql,null);
        $json       = json_decode($querys, true);
        $errorInfo  = $json['errorInfo'];
        $dataCount  = $json['dataCount'];
        $rows       = $json['data'];

        for($i=0 ; $i < $dataCount ; $i++) {
          $bill_id      = $rows[$i]['bill_id'];
          $vendor_code  = $rows[$i]['vendor_code'];
          $vendor_name  = $rows[$i]['vendor_name'];
          $doc_no       = empty($rows[$i]['doc_no'])?"-":$rows[$i]['doc_no'];
          $doc_date     = is_null($rows[$i]['doc_date'])?"-":DateThai($rows[$i]['doc_date']);
          $due_date     = is_null($rows[$i]['due_date'])?"-":DateThai($rows[$i]['due_date']);
          $receive_date = DateThai($rows[$i]['receive_date']);
          $create_date  = DateThai($rows[$i]['create_date']);
          $status       = $rows[$i]['status'];
          $total        = number_format($rows[$i]['total'],2);
        ?>
        <tr>
        <td class="font-9 table-bordered text-center"><?= $i+1?></td>
        <td class="font-9 table-bordered text-center"><?= $doc_no ?></td>
        <td class="font-9 table-bordered text-center"><?= $receive_date ?></td>
        <td class="font-9 table-bordered text-right"><?= $total ?></td>
        <td class="font-9 table-bordered"><?= $vendor_name ?></td>
        <td class="font-9 table-bordered" text-center><?= $due_date ?></td>
      </tr>
    <?php } ?>
      </tbody>
    </table>
  </body>
</html>
<?php
  $html = ob_get_contents();
  ob_end_clean();
  $stylesheet = file_get_contents('../css/print.css');
  $mpdf->SetTitle('Billing Note Report');
  // $mpdf->showWatermarkText = true;
  $mpdf->WriteHTML($stylesheet,\Mpdf\HTMLParserMode::HEADER_CSS);
  $mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);
  // $mpdf->Output('IVRENT201711000059.pdf', \Mpdf\Output\Destination::DOWNLOAD);
  $mpdf->Output($_reserve_code.'.pdf', \Mpdf\Output\Destination::INLINE);
?>
