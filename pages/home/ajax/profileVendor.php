<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$con = "";
$member = $_SESSION['member'];

print_r($member);


?>
<div class="box-body box-profile">
  <img class="profile-user-img img-responsive img-circle" src="<?= $member['0']['user_img']; ?>" alt="User profile picture">

  <h3 class="profile-username text-center"><?= $member['0']['user_name']; ?></h3>


  <ul class="list-group list-group-unbordered">
    <li class="list-group-item">
      <b>Followers</b> <a class="pull-right">1,322</a>
    </li>
    <li class="list-group-item">
      <b>Following</b> <a class="pull-right">543</a>
    </li>
    <li class="list-group-item">
      <b>Friends</b> <a class="pull-right">13,287</a>
    </li>
  </ul>

  <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>
</div>
