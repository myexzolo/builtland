<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


if(!isset($_SESSION))
{
    session_start();
}

$status       = isset($_POST['status'])?$_POST['status']:"";
$bill_id      = isset($_POST['id'])?$_POST['id']:"";

?>
<input type="hidden" name="status" value="<?=$status?>">
<input type="hidden" name="id" value="<?=$bill_id?>">
<div class="modal-body">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group">
        <label>หมายเหตุ</label>
        <textarea class="form-control" rows="3" name="note" placeholder="สำหรับเจ้าหน้าที่ ..."></textarea>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;">บันทึก</button>
</div>
