<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$con = "";

$date_start = isset($_POST['date_start'])?$_POST['date_start']:"";
$date_end   = isset($_POST['date_end'])?$_POST['date_end']:"";
$type       = isset($_POST['type'])?$_POST['type']:"";
$vendor_code  = isset($_POST['vendor_code'])?$_POST['vendor_code']:"";

$member = $_SESSION['member'];
$role_list = $member[0]['role_list'];
$roleArr   = explode(",",$role_list);

$display = "";
if (in_array("999", $roleArr)) {
  $display = "style='display:none'";
}

?>
<style>
.label2 {
  display: block;
  padding: .2em .6em .2em;
  font-weight: 400;
  line-height: 20px;
  color: #fff;
  text-align: center;
  white-space: nowrap;
  vertical-align: baseline;
  border-radius: .25em;
  width: 85px;
}
</style>
<table class="table table-bordered table-striped table-hover" id="tableBill" style="min-width:1000px;width:100%">
  <thead>
    <tr class="text-center">
      <th style="width:30px">No.</th>
      <th>วันที่ส่งของ</th>
      <th>วันที่วางบิล</th>
      <th <?= $display ?>>Vendor.Name</th>
      <th>Doc.No</th>
      <th>Doc.date</th>
      <th>วันที่รับเช็ค</th>
      <th style="width:80px">สถานะ</th>
      <th style="width:120px">ยอดรวม</th>
      <?php if($_SESSION['ROLE_USER']['is_print']){ ?><th style="width:40px">พิมพ์</th><?php }?>
      <th style="width:40px">ดู</th>
      <?php if($_SESSION['ROLE_USER']['is_update']){ ?><th style="width:40px">แก้ไข</th><?php }?>
      <?php if($_SESSION['ROLE_USER']['is_delete']){ ?><th style="width:40px">ลบ</th><?php }?>
    </tr>
  </thead>
  <tbody>
    <?php
      $date_start = isset($_POST['date_start'])?$_POST['date_start']:"";
      $date_end   = isset($_POST['date_end'])?$_POST['date_end']:"";
      $type       = isset($_POST['type'])?$_POST['type']:"";
      $vendor_code   = isset($_POST['vendor_code'])?$_POST['vendor_code']:"";
      $con = "";

      if($date_start != "")
      {
        $con .= " and b.receive_date between '$date_start' and '$date_end' ";
      }

      if($type != "")
      {
        $con .= " and b.status = '$type' ";
      }

      if($vendor_code != "")
      {
        $con .= " and b.vendor_code = '$vendor_code' ";
      }

      $sql ="SELECT b.*, v.vendor_name FROM t_bill b,t_vendor v  where b.vendor_code = v.vendor_code $con order by update_date";
      //echo $sql;
      $querys     = DbQuery($sql,null);
      $json       = json_decode($querys, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];

      for($i=0 ; $i < $dataCount ; $i++) {
        $bill_id      = $rows[$i]['bill_id'];
        $vendor_code  = $rows[$i]['vendor_code'];
        $vendor_name  = $rows[$i]['vendor_name'];
        $doc_no       = empty($rows[$i]['doc_no'])?"-":$rows[$i]['doc_no'];
        $doc_date     = is_null($rows[$i]['doc_date'])?"-":DateThai($rows[$i]['doc_date']);
        $due_date     = is_null($rows[$i]['due_date'])?"-":DateThai($rows[$i]['due_date']);
        $receive_date = DateThai($rows[$i]['receive_date']);
        $create_date  = DateThai($rows[$i]['create_date']);
        $status       = $rows[$i]['status'];
        $total        = number_format($rows[$i]['total'],2);


        $onclickPrint   = "";
        $onclickEdit    = "";
        $onclickDelete  = "";

        $disable  = "";
        $displayPrint = " style='display:none'";

        if($status == "W"){
          $status = "<span class='label2 label-warning'>รออนุมัติ</span>";
        }elseif($status == "N"){
          $status = "<span class='label2 label-danger'>ไม่อนุมัติ</span>";
        }elseif($status == "A"){
          $status = "<span class='label2 label-success'>อนุมัติ</span>";
          $onclickEdit    = "";
          $onclickDelete  = "";
          $disable  = "disable";
          $displayPrint = "";
          $onclickPrint = " onclick=printDoc('$bill_id','$doc_no')";
        }elseif($status == "C"){
          $status = "<span class='label2 label-default'>ยกเลิก</span>";
        }elseif($status == "D"){
          $status = "<span class='label2 bg-gray'>ลบข้อมูล</span>";
        }

      ?>
      <tr>
        <td align="center"><?= $i+1;?></td>
        <td align="center"><?= $receive_date; ?></td>
        <td align="center"><?= $create_date; ?></td>
        <td <?= $display ?>><?= $vendor_name; ?></td>
        <td align="center"><?= $doc_no; ?></td>
        <td align="center"><?= $doc_date; ?></td>
        <td align="center"><?= $due_date; ?></td>
        <td align="center"><?= $status; ?></td>
        <td align="right"><?= $total; ?></td>
        <?php if($_SESSION['ROLE_USER']['is_print']){ ?><td align="center"><a class="btn_point text-green" <?= $displayPrint.$onclickPrint ?>><i class="fa fa-print" onclick=""></i></a></td ><?php }?>
        <td align="center"><a class="btn_point text-teal"><i class="fa fa-search" onclick="showForm('VIEW','<?= $bill_id ?>')"></i></a></td ></td>
        <?php if($_SESSION['ROLE_USER']['is_update']){ ?><td align="center"><a class="btn_point" <?=$disable ?>><i class="fa fa-edit" onclick="showForm('EDIT','<?= $bill_id ?>')"></i></a></td ><?php }?>
        <?php if($_SESSION['ROLE_USER']['is_delete']){ ?><td align="center"><a class="btn_point text-red" <?=$disable ?>><i class="fa fa-trash-o" onclick="del('<?=$bill_id ?>','<?=$value['user_name']?>')"></i></a></td ><?php }?>
      </tr>
    <?php
      }
    ?>
  </tbody>
</table>

<script>
  $(function () {
    $('#tableBill').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false,
      'bDestroy'    : true,
      'oLanguage': {
        'sEmptyTable': 'ไม่พบข้อมูล'
      }
    }).columns.adjust();
  })
</script>
