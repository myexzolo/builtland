<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');

$action         = isset($_POST['action'])?$_POST['action']:"";
$vendor_code    = isset($_POST['vendor_code'])?$_POST['vendor_code']:"";
$bill_id        = isset($_POST['bill_id'])?$_POST['bill_id']:"";
$doc_no         = isset($_POST['doc_no'])?$_POST['doc_no']:"";
$doc_date       = isset($_POST['doc_date'])?$_POST['doc_date']:"";
$due_date       = isset($_POST['due_date'])?$_POST['due_date']:"";
$receive_date   = isset($_POST['receive_date'])?$_POST['receive_date']:"";
$total          = isset($_POST['total'])?str_replace(",","",$_POST['total']):"";


$bill_detail_id = isset($_POST['bill_detail_id'])?$_POST['bill_detail_id']:null; //array
$po_no          = isset($_POST['po_no'])?$_POST['po_no']:null; //array
$amount         = isset($_POST['amount'])?$_POST['amount']:null; //array
//$file           = isset($_POST['file'])?$_POST['file']:null; //array
$path_file      = isset($_POST['path_file'])?$_POST['path_file']:null; //array
$id_delete      = isset($_POST['id_delete'])?$_POST['id_delete']:null; //array


$user_id_update = $_SESSION['member'][0]['user_id'];

if($receive_date != ""){
  $receive_date  = dateThToEn($receive_date,"dd/mm/yyyy","/");
}

//print_r($_POST);
if($due_date != ""){
  $due_date  = dateThToEn($due_date,"dd/mm/yyyy","/");
}

$sql = "";
$dateTime = date('Y/m/d H:i:s');

$arr = array();
if($action == "ADD"){
  $arr['doc_no']           = $doc_no;
  $arr['vendor_code']      = $vendor_code;
  // $arr['doc_date']         = $doc_date;
  $arr['due_date']         = $due_date;
  $arr['receive_date']     = $receive_date;
  $arr['status']           = 'W';
  $arr['total']            = $total;
  $arr['create_by']        = $user_id_update;
  $arr['create_date']      = $dateTime;

  $sql = DBInsertPOST($arr,'t_bill');
  //echo $sql;
  $query      = DbQuery($sql,null);
  $row        = json_decode($query, true);
  $errorInfo  = $row['errorInfo'];
  $bill_id    = $row['id'];

  if(intval($errorInfo[0]) == 0){
    $sql = "";
    for($x= 0; $x < count($po_no); $x++) {
      $arr = array();
      $arr['bill_id']     = $bill_id;
      $arr['po_no']       = $po_no[$x];
      $arr['amount']      = $amount[$x];
      $arr['create_by']   = $user_id_update;
      $arr['create_date'] = $dateTime;

      $pathFile           = $path_file[$x];
      //echo ">>>".$pathFile;

      $file       = $_FILES['file'];
      if(trim($file["tmp_name"][$x]) != "")
      {
          $path    = "../../../file/po/".$vendor_code."/";

          if(!is_dir($path))
          {
              mkdir($path, 0777, true);
          }

          if($pathFile!= "" && !is_dir($pathFile))
          {
              unlink($pathFile);
          }

          $name = $po_no[$x]."_".time();
          $fileUp["name"]       = $file["name"][$x];
          $fileUp["type"]       = $file["type"][$x];
          $fileUp["tmp_name"]   = $file["tmp_name"][$x];
          $fileUp["error"]      = $file["error"][$x];
          $fileUp["size"]       = $file["size"][$x];

          $uploadfile = uploadfile($fileUp,$path,$name);
          $arr['path']   = "file/po/".$vendor_code."/".$uploadfile['name'];

      }else{
        $arr['path']   = $pathFile;
      }
      $sql .= DBInsertPOST($arr,'t_bill_detail');
    }
  }else{
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'danger','message' => 'Fail : '.$errorInfo[2])));
  }
}else if("EDIT"){
  // $arr['doc_no']           = $doc_no;
  // $arr['vendor_code']      = $vendor_code;
  $arr['due_date']         = $due_date;
  $arr['receive_date']     = $receive_date;
  $arr['total']            = $total;
  $arr['update_by']        = $user_id_update;
  $arr['bill_id']          = $bill_id;

  $sql .= DBUpdatePOST($arr,'t_bill','bill_id');

  if($id_delete != null)
  {
    for($x= 0; $x < count($id_delete); $x++)
    {
      $id   = $id_delete[$x];
      $sqlb = "select * FROM t_bill_detail WHERE bill_detail_id = '$id'";

      $queryb      = DbQuery($sqlb,null);
      $jsonb       = json_decode($queryb, true);
      $rowb        = $jsonb['data'];

      $pathb       = $rowb[0]['path'];
      $pathFileB   = "../../../".$pathb;

      if(file_exists($pathFileB))
      {
          unlink($pathFileB);
      }
      $sql .= "delete FROM t_bill_detail WHERE bill_detail_id = '$id';";
    }
  }

  for($x= 0; $x < count($po_no); $x++) {
    $arr = array();
    $arr['bill_id']         = $bill_id;
    $arr['po_no']           = $po_no[$x];
    $arr['amount']          = $amount[$x];
    $billDetailId           = $bill_detail_id[$x];

    $pathFile               = $path_file[$x];
    $pathFile_tmp           = "../../../".$pathFile;


    $file       = $_FILES['file'];
    if(trim($file["tmp_name"][$x]) != "")
    {
        $path    = "../../../file/po/".$vendor_code."/";

        // echo $pathFile_tmp;
        // echo var_dump(file_exists($pathFile_tmp));
        if($pathFile != "" && file_exists($pathFile_tmp))
        {
            unlink($pathFile_tmp);
        }

        $name = $po_no[$x]."_".time();
        $fileUp["name"]       = $file["name"][$x];
        $fileUp["type"]       = $file["type"][$x];
        $fileUp["tmp_name"]   = $file["tmp_name"][$x];
        $fileUp["error"]      = $file["error"][$x];
        $fileUp["size"]       = $file["size"][$x];

        $uploadfile = uploadfile($fileUp,$path,$name);
        $arr['path']   = "file/po/".$vendor_code."/".$uploadfile['name'];

    }else{
      $arr['path']   = $pathFile;
    }

    if($billDetailId != "")
    {
      $arr['bill_detail_id']  = $billDetailId;
      $arr['update_by']       = $user_id_update;

      $sql .= DBUpdatePOST($arr,'t_bill_detail','bill_detail_id');
    }else{
      $arr['create_by']       = $user_id_update;
      $arr['create_date']     = $dateTime;

      $sql .= DBInsertPOST($arr,'t_bill_detail');
    }
  }
}

//echo  $sql;
$query      = DbQuery($sql,null);
$row        = json_decode($query, true);
$errorInfo  = $row['errorInfo'];


if(intval($errorInfo[0]) == 0){
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'Success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail : '.$errorInfo[2].",".$sql)));
}


?>
