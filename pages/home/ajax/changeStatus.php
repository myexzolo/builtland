<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');

if(!isset($_SESSION))
{
    session_start();
}

$status       = isset($_POST['status'])?$_POST['status']:"";
$bill_id      = isset($_POST['id'])?$_POST['id']:"";
$note         = isset($_POST['note'])?$_POST['note']:"";

$user_id_update = $_SESSION['member'][0]['user_id'];

$date   = date('Y/m/d');
$year   = date('y');

$message = "";
$arr = array();

if($status == 'A'){

  $sql        = "SELECT doc_no  FROM t_bill where  bill_id = '$bill_id'";
  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $row        = $json['data'];
  $doc_no     = $row[0]['doc_no'];

  if($doc_no == "")
  {
    $docNo = "BLN-".$year;

    $sql        = "SELECT MAX(doc_no) as doc_no FROM t_bill where  doc_no like '$docNo%'";
    $query      = DbQuery($sql,null);
    $json       = json_decode($query, true);
    $row        = $json['data'];
    $doc_no_tmp = $row[0]['doc_no'];

    if($doc_no_tmp != null && $doc_no_tmp != "")
    {
        $num  = intval(substr($doc_no_tmp,strlen($docNo)));
        $doc_no   = $docNo.sprintf("%05d",($num + 1));
    }else
    {
      $doc_no = $docNo.sprintf("%05d", 1);
    }
  }

  $arr['update_by']        = $user_id_update;
  $arr['doc_no']         = $doc_no;
  $arr['doc_date']         = $date;
  $arr['status']           = $status;
  $arr['bill_id']          = $bill_id;

  $sql = DBUpdatePOST($arr,'t_bill','bill_id');

  $message = "เปลี่ยนแปลงสถานะ อนุมัติ สำเร็จ";
}
else if($status == 'N')
{
  $arr['update_by']        = $user_id_update;
  $arr['status']           = $status;
  $arr['note']             = $note;
  $arr['bill_id']          = $bill_id;

  $sql = DBUpdatePOST($arr,'t_bill','bill_id');
  $message = "เปลี่ยนแปลงสถานะ ไม่อนุมัติ สำเร็จ";
}
else if($status == 'C')
{
  $arr['update_by']        = $user_id_update;
  $arr['status']           = $status;
  $arr['note']             = $note;
  $arr['bill_id']          = $bill_id;

  $sql = DBUpdatePOST($arr,'t_bill','bill_id');
  $message = "เปลี่ยนแปลงสถานะ ยกเลิก สำเร็จ";
}
else if($status == 'R')
{
  $arr['doc_no']           = "";
  $arr['doc_date']         = null;
  $arr['update_by']        = $user_id_update;
  $arr['status']           = "W";
  $arr['note']             = $note;
  $arr['bill_id']          = $bill_id;

  $sql = DBUpdatePOST($arr,'t_bill','bill_id');
  $message = "เปลี่ยนแปลงสถานะ รีเซ็ต สำเร็จ";
}


$query      = DbQuery($sql,null);
$row        = json_decode($query, true);
$errorInfo  = $row['errorInfo'];


if(intval($errorInfo[0]) == 0){
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => $message)));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail : '.$errorInfo[2].",".$sql)));
}
?>
