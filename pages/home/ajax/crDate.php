<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');

if(!isset($_SESSION))
{
    session_start();
}

// $date         = date('Y/m/d');
$waybill_date = isset($_POST['waybill_date'])?$_POST['waybill_date']:"";
$date         = isset($_POST['bill_date'])?$_POST['bill_date']:"";
$term         = isset($_POST['term'])?$_POST['term']:"";

// $date         = "2020/04/21";
// $waybill_date = "2020/04/17";
// $term         = "15";


$sql   = "SELECT * FROM t_bill_date where waybill_date >= '$waybill_date'";
//echo $sql;
$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$row        = $json['data'];
$dataCount  = $json['dataCount'];

$dateCr     = "";

for($i=0 ; $i < $dataCount ; $i++)
{
  $bill_date      = $row[$i]['bill_date'];
  $dif = DateDiff($date,$bill_date);
  // echo ">>>>".$dif;
  if($dif >= 0)
  {
      if($term == 15){
        $dateCr = DateThai($row[$i]['cr_15']);
      }
      else if($term == 30)
      {
        $dateCr = DateThai($row[$i]['cr_30']);
      }
      else if($term == 40)
      {
        $dateCr = DateThai($row[$i]['cr_40']);
      }
      else if($term == 60)
      {
        $dateCr = DateThai($row[$i]['cr_60']);
      }
      else if($term == 90)
      {
        $dateCr = DateThai($row[$i]['cr_90']);
      }

      break;
  }
}

header('Content-Type: application/json');
exit(json_encode(array('status' => true,'message' => $dataCount,'dateCr'=> $dateCr)));
?>
