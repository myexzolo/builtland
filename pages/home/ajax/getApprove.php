<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');

if(!isset($_SESSION))
{
    session_start();
}

$status       = $_POST['status'];
$vendor_code  = $_POST['vendor_code'];
$con          = "";

if($vendor_code != "")
{
   $con = " and vendor_code='$vendor_code' ";
}
$date   = date('Y/m/d');

$sql   = "SELECT * FROM t_bill where status = '$status' $con order by create_date";

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$row        = $json['data'];
$dataCount  = $json['dataCount'];

$startDate = "";
$endDate = "";
if($dataCount > 0){
  $startDate  = date("d/m/Y", strtotime($row[0]['create_date']));
  $endDate    = date('t/m/Y');
}


header('Content-Type: application/json');
exit(json_encode(array('status' => true,'message' => $dataCount,'startDate'=> $startDate,'endDate'=> $endDate)));
?>
