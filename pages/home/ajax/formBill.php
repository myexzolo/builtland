<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action         = $_POST['value'];
$vendor_code    = $_SESSION['member'][0]['user_login'];
$bill_id        = isset($_POST['bill_id'])?$_POST['bill_id']:"";



$date_now       = date("Y/m/d");
$receive_date   = DateThai($date_now);
$bill_date      = $receive_date;

$role_list = $_SESSION['member'][0]['role_list'];
$roleArr   = explode(",",$role_list);


// $vendor_code          = "";
// $vendor_name          = "";
// bill_id
$doc_no   = "";
// vendor_code
$doc_date = "";
$due_date = "";
// receive_date
// status
// total
// create_by
// create_date
// update_by
// update_date
// note
$vendor_name = "";
$term   = "";


$readonly = "";
$disabled = "";
$disabledStaffonly = "disabled";
$display  = "";
$finance  = "";
$displayterm = "style='display:none'";

$btn      = 'ยกเลิก';
$backgroundColor = "background-color:#fff;";
$openDate = "";
$openDueDate = "";
$dueDateText = "วันที่รับเช็ค";
$col = "12";

$note = "";

if (in_array("2", $roleArr) || in_array("1", $roleArr) || in_array("0", $roleArr)) {
  if($_SESSION['ROLE_USER']['is_approve'])
  {
    $finance = "Y";
    $openDate     = "onclick=\"openDate('datepicker')\"";
    $openDueDate  = "onclick=\"openDate('due_date')\"";
    $displayterm  = "";
    $dueDateText  = "วันที่ครบกำหนด";
  }
}

if($action == "VIEW" || $action == 'EDIT')
{
  $sqlbl ="SELECT b.*, v.vendor_name,v.term FROM t_bill b, t_vendor v  where b.bill_id = '$bill_id' and b.vendor_code = v.vendor_code";
  //echo $sql;
  $querybl    = DbQuery($sqlbl,null);
  $jsonbl     = json_decode($querybl, true);
  $rowbl      = $jsonbl['data'];

  $receive_date = DateThai($rowbl[0]['receive_date']);
  $doc_no     = isset($rowbl[0]['doc_no'])?$rowbl[0]['doc_no']:"";
  $doc_date   = is_null($rowbl[0]['doc_date'])?"":DateThai($rowbl[0]['doc_date']);
  $due_date   = is_null($rowbl[0]['due_date'])?"":DateThai($rowbl[0]['due_date']);
  $note       = is_null($rowbl[0]['note'])?"":$rowbl[0]['note'];
  $bill_date  = DateThai($rowbl[0]['create_date']);

  $vendor_name  = $rowbl[0]['vendor_name'];
  $term         = $rowbl[0]['term'];

}


if($action == "VIEW")
{
  $openDate =  "";
  $openDueDate = "";
  $readonly = "readonly";
  $disabled = "disabled";
  $disabledStaffonly = 'disabled';
  $display  = "display:none;";
  $btn = 'ปิด';
  $backgroundColor = "";
}
else if($action == 'EDIT')
{
  $col = "10";
  if($finance == "Y") {
      $disabledStaffonly = "";
  }
  // $readonly = "readonly";
}
else if($action == 'ADD'){
  $sqlv ="SELECT * FROM t_vendor where vendor_code = '$vendor_code'";
  //echo $sql;
  $queryv    = DbQuery($sqlv,null);
  $jsonv     = json_decode($queryv, true);
  $rowv      = $jsonv['data'];

  $vendor_name  = $rowv[0]['vendor_name'];
  $term         = $rowv[0]['term'];
}
?>
<input type="hidden" id="action" name="action" value="<?=$action?>">
<input type="hidden" name="vendor_code" value="<?=$vendor_code?>">
<input type="hidden" name="bill_id" value="<?=$bill_id?>">
<div class="modal-body">
  <div class="row">
      <?php if($finance == "Y"){ ?>
        <div class="col-md-2">
            <div class="form-group">
              <label>เลขที่เอกสาร</label>
              <input class="form-control" <?= $disabled ?>  value="<?= $doc_no ?>" name="doc_no" type="text" readonly>
          </div>
        </div>
        <div class="col-md-8">
          <div class="form-group">
            <label>Vendor Name</label>
            <input value="<?=$vendor_name?>" <?= $disabled ?>  name="vendor_name" type="text" class="form-control" placeholder="Name"  required data-smk-msg="&nbsp;">
          </div>
        </div>
      <?php } ?>
      <div class="col-md-2" <?=$displayterm ?>>
        <div class="form-group">
          <label>เครดิต (วัน)</label>
          <input value="<?=$term ?>" <?= $disabled ?> id="term" name="term" type="text" class="form-control"  required  style="text-align:right;" data-smk-msg="&nbsp;">
        </div>
      </div>
      <?php if($finance == "Y"){ ?>
      <div class="col-md-3">
          <div class="form-group">
            <label>วันที่ออกเอกสาร</label>
            <div class="date">
                  <input class="input-medium form-control" <?= $disabled ?> required value="<?= $doc_date ?>" name="doc_date" id="datepicker2" type="text" data-provide="datepicker" data-date-language="th-th" readonly style="<?=$backgroundColor?>">
            </div>
        </div>
      </div>
    <?php } ?>
      <div class="col-md-3">
          <div class="form-group">
            <label>วันที่ส่งของ</label>
            <div class="date">
                  <input class="input-medium form-control" <?= $disabled ?>  value="<?= $receive_date ?>"
                  onchange="crDate(this)" name="receive_date" id="datepicker" type="text" data-provide="datepicker" data-date-language="th-th"
                  readonly style="<?=$backgroundColor?>">
            </div>
        </div>
      </div>
      <div class="col-md-3">
          <div class="form-group">
            <label>วันที่วางบิล</label>
            <div class="date">
              <?php if($finance == "Y"){ ?>
                <input class="input-medium form-control" <?= $disabled ?> onchange="crDate(this)" value="<?= $bill_date ?>" name="bill_date"
                id="bill_date" type="text" data-provide="datepicker" data-date-language="th-th" readonly style="<?=$backgroundColor?>">
              <?php }else { ?>
                <input class="form-control" value="<?= $bill_date ?>" name="bill_date" id="bill_date" type="text" readonly style="<?=$backgroundColor?>">
              <?php } ?>

            </div>
        </div>
      </div>
    <div class="col-md-3">
        <div class="form-group">
          <label><?=$dueDateText?></label>
          <div class="date">
        <?php if($finance == "Y"){ ?>
          <input class="input-medium form-control" <?= $disabled ?>  value="<?= $due_date ?>"
          name="due_date" id="due_date" type="text" data-provide="datepicker" data-date-language="th-th"
          readonly style="<?=$backgroundColor?>">
        <?php }else { ?>
          <input class="form-control" value="<?= $due_date ?>" name="due_date" id="due_date" type="text" readonly style="<?=$backgroundColor?>">
        <?php } ?>
          </div>
      </div>
    </div>
      <div class="col-md-12" style="margin-bottom:5px;<?= $display?>">
        <button type="button" style="width:100px" class="btn btn-sm btn-info btn-flat pull-right"  onclick="addRow()"><i class="fa fa-plus"></i> เพิ่ม</button>
      </div>
      <div class="col-md-12">
        <table class="table table-bordered table-striped" id="tablePo" style="width:100%;margin-bottom: 5px;">
          <thead>
            <tr>
              <th style="width:40px;<?= $display?>"></th>
              <th>PO/WO NO.</th>
              <th style="width:150px;">จำนวนเงิน</th>
              <th style="width:300px;">แนบไฟล์</th>
            </tr>
          </thead>
          <tbody>
            <?php
                $det_id     = "";
                $po_no      = "";
                $amount     = "";
                $path_file  = "";


                if($action == "VIEW" || $action == 'EDIT'){
                $sql ="SELECT * FROM t_bill_detail where bill_id = '$bill_id' order by bill_detail_id";
                //echo $sql;
                $querys     = DbQuery($sql,null);
                $json       = json_decode($querys, true);
                $errorInfo  = $json['errorInfo'];
                $dataCount  = $json['dataCount'];
                $rows       = $json['data'];

                if($path_file != "" && $action == "EDIT"){
                  $col = "10";
                }

                for($i=0 ; $i < $dataCount ; $i++) {
                  $bill_id        = $rows[$i]['bill_id'];
                  $bill_detail_id = $rows[$i]['bill_detail_id'];
                  $po_no          = $rows[$i]['po_no'];
                  $amount         = $rows[$i]['amount'];
                  $path           = $rows[$i]['path'];

              ?>
              <tr>
                <td align="center" style="<?= $display?>">
                    <a class="btn_point text-red" style="line-height:40px;"><i class="fa fa-trash-o" onclick="delRow(this,'<?= $bill_detail_id ?>')"></i></a>
                </td>
                <td>
                  <div class="form-group" style="margin-bottom: 0px;">
                    <input value="<?=$bill_detail_id ?>" name="bill_detail_id[]" type="hidden" class="bill_detail">
                    <input value="<?=$po_no?>" <?=$disabled?> name="po_no[]" type="text" data-smk-msg="&nbsp;" class="form-control" placeholder="เลขที่วางบิล" required>
                  </div>
                </td>
                <td><div class="form-group"  style="margin-bottom: 0px;">
                  <input value="<?=$amount?>" <?=$disabled?> name="amount[]" type="text" onkeyup="checknumber(this);cal();" data-smk-msg="&nbsp;" class="form-control text-right amount" onchange="cal()"  placeholder="0.00" required></div></td>
                <td>
                  <div class="form-group"  style="margin-bottom: 0px;">
                    <div class="row file_path">
                      <div class="col-md-<?=$col ?>" align="center">
                        <?php if($action == "VIEW"){
                          ?>
                          <a class="btn_point text-success" style="line-height:40px;" href=<?= "../../".$path?> target="_blank" >
                            <i class="fa fa-file-pdf-o"></i>
                          </a>
                          <?php
                        }else{?>
                        <input name="file[]" type="file" class="form-control custom-file-input" onchange="readURL(this,'path_<?= $i; ?>');" accept="application/pdf">
                        <input type="hidden" value="<?=$path?>" name="path_file[]">
                        <input type="hidden" value="<?=$path?>" data-smk-msg="&nbsp;" class="path" id="path_<?= $i; ?>" required>
                        <?php } ?>
                      </div>
                      <?php
                      if($path != "" && $action == "EDIT"){
                      ?>
                        <div class="col-md-2" class="showImg">
                            <a class="btn_point text-success" style="line-height:40px;" href=<?= "../../".$path?> target="_blank" >
                              <i class="fa fa-file-pdf-o"></i>
                            </a>
                      </div>
                      <?php
                      }
                      ?>
                  </div>
                 </div>
                </td>
              </tr>
              <?php
                }
              }
              else
              {
            ?>
            <tr>
              <td align="center" style="<?= $display?>">
                  <a class="btn_point text-red" style="line-height:40px;"><i class="fa fa-trash-o" onclick="delRow(this,'')"></i></a>
              </td>
              <td>
                <div class="form-group" style="margin-bottom: 0px;">
                  <input value="" name="po_no[]" type="text" data-smk-msg="&nbsp;" class="form-control" placeholder="เลขที่วางบิล" required>
                </div>
              </td>
              <td><div class="form-group"  style="margin-bottom: 0px;">
                <input value="" name="amount[]" type="text" data-smk-msg="&nbsp;" class="form-control text-right amount" onchange="cal()" onkeyup="checknumber(this);cal()" placeholder="0.00" required></div></td>
              <td>
                <div class="form-group"  style="margin-bottom: 0px;">
                  <div class="row">
                    <div class="col-md-<?=$col ?>">
                      <input name="file[]" type="file" class="form-control custom-file-input" onchange="readURL(this,'path_1');" accept="application/pdf">
                      <input type="hidden" value="" name="path_file[]">
                      <input type="hidden" value="" data-smk-msg="&nbsp;" class="path" id="path_1" required>
                    </div>
                </div>
               </div>
              </td>
            </tr>
          <?php } ?>
          </tbody>
        </table>
        <div style="text-align:right;<?=$display?>">
          <span style="font-size:18px;color:#222;">*** แนบไฟล์ PDF เท่านั้น ขนาดไม่เกิน 2M<span>
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <label>หมายเหตุ</label>
          <textarea class="form-control" rows="3" name="note" placeholder="สำหรับเจ้าหน้าที่ ..." <?= $disabledStaffonly ?>><?= $note ?></textarea>
        </div>
      </div>
      <div class="col-md-12" style="margin-bottom:5px;">
         <div class="pull-right" style="width:200px">
           <input value="" name="total" id="total" type="text" style="text-align:right;font-size:24px;<?=$backgroundColor?>" <?= $disabled ?>
                           data-smk-msg="&nbsp;" class="form-control" placeholder="0.00" required readonly>
         </div>
         <div class="pull-right"<label style="font-size:24px;padding-right:10px;line-height:40px;" ><b>ยอดรวม</b></label></div>
      </div>
      <div>
      <div id="tmp_file" style="display:none">
      <div class="col-md-12" align="center">
        <input name="file[]" type="file" class="form-control custom-file-input" accept="application/pdf">
        <input type="hidden" value="" name="path_file[]">
        <input type="hidden" value="" data-smk-msg="&nbsp;" class="path" >
      </div>
    </div>
    <div id="id_delete"></div>
</div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal"><?=$btn?></button>
    <button type="submit" class="btn btn-primary btn-flat" style="width:100px;<?=$display?>">บันทึก</button>
</div>
