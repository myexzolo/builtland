<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$companycode  = $_SESSION['branchCode'];
?>
<style>
.thumbnail {
  border: 1px solid #ddd; /* Gray border */
  border-radius: 4px;  /* Rounded border */
  padding: 5px; /* Some padding */
  width: 100px; /* Set a small width */
}
th {
  text-align: center;
}
</style>
<table class="table table-bordered table-hover table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th>Package</td>
      <th>รหัส</th>
      <th>ชื่อเล่น</th>
      <th>ชื่อ - สกุล</th>
      <th>วันเดือนปีเกิด</th>
      <th>เบอร์โทร</th>
      <th>วันที่เริ่มงาน</th>
      <th>วันหมดอายุ</th>
      <th>สถานะ</th>
      <th>ผู้ดูแล</th>
      <th>Edit</th>
      <th>Del</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $sql = "SELECT p.*,e.EMP_NICKNAME,e.EMP_CODE FROM  person p LEFT JOIN data_mas_employee e
            ON p.EMP_CODE_SALE =e.EMP_CODE
            where p.PERSON_CODE != '0000' and p.COMPANY_CODE ='$companycode'
            and p.PERSON_STATUS != 'D'  ORDER BY PERSON_LAST_VISIT DESC";

    //echo $sql;
    $query      = DbQuery($sql,null);
    $json       = json_decode($query, true);
    $errorInfo  = $json['errorInfo'];
    $rows       = $json['data'];
    $dataCount  = $json['dataCount'];

    for($i=0 ; $i < $dataCount ; $i++) {

      $emp_code = $rows[$i]['EMP_CODE_SALE'];
      if(!empty($rows[$i]['EMP_NICKNAME'])){
        $empName  = $rows[$i]['EMP_NICKNAME']." ($emp_code)";
      }else{
        $empName = "";
      }

      $cusName  = $rows[$i]['PERSON_TITLE'].$rows[$i]['PERSON_NAME']." ".$rows[$i]['PERSON_LASTNAME'];

      $fanme = $rows[$i]['PERSON_CODE']." : ".$cusName." (".$rows[$i]['PERSON_NICKNAME'].")";

    ?>
    <tr class="text-center">
      <td><button type="button" class="btn btn-default btn-sm btn-flat" onclick="showPackage('<?= $rows[$i]['PERSON_CODE'];?>','<?=$fanme; ?>')"><i class="fa fa-tags" aria-hidden="true"></i></button></td>
      <td><?=$rows[$i]['PERSON_CODE'];?></td>
      <td align="left"><?=$rows[$i]['PERSON_NICKNAME'];?></td>
      <td align="left"><?= $cusName?></td>
      <td><?= formatDateTh($rows[$i]['PERSON_BIRTH_DATE']);?></td>
      <td align="left"><?= $rows[$i]['PERSON_TEL_MOBILE'];?></td>
      <td><?= formatDateTh($rows[$i]['PERSON_REGISTER_DATE']);;?></td>
      <td><?= formatDateTh($rows[$i]['PERSON_EXPIRE_DATE']);?></td>
      <td><?= $rows[$i]['PERSON_STATUS'];?></td>
      <td align="left"><?= $empName ?></td>
      <td>
        <button type="button" class="btn btn-warning btn-sm btn-flat" onclick="showForm('EDIT','<?=$rows[$i]['PERSON_CODE']?>')">Edit</button>
        </td>
      <td>
        <button type="button" class="btn btn-danger btn-sm btn-flat" onclick="del('<?=$rows[$i]['PERSON_CODE']?>','<?=$cusName?>')">Del</button>
      </td>
    </tr>
    <?php } ?>
  </tbody>
</table>
<script>
$(function () {
  $('#tableDisplay').DataTable({
   'paging'      : true,
   'lengthMenu'  : [10,20,50,100],
   'lengthChange': true,
   'searching'   : true,
   'ordering'    : false,
   'info'        : true,
   'autoWidth'   : false
 })
})
</script>
