<?php

include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action       = $_POST['value'];
$id           = $_POST['id'];
$branch_code  = $_SESSION['branchCode'];

// $title      = "";
// $content    = "";
// $web        = "https://www.gymmonkeybkk.com/payment/index.php";
// $img        = "";
// $img_web    = "";
// $start_date   = "";
// $end_date     = "";
// $price        = "";
// //$branch_code  = "";
// $is_active    = "";


$package_id         = "";
$package_code       = "";
$package_name       = "";
$package_detail     = "";
$package_type       = "";
$num_use            = "";
$package_unit       = "";
$max_use            = "";
$package_price      = "";
$type_vat           = "";
$notify_num         = "";
$notify_unit	      = "";
$package_status     = "";
$company_code       = "";
$branch1            = "";
$branch2            = "";
$branch3            = "";
$branch4            = "";
$branch5            = "";
$promotion          = "";
$start_pro_date     = "";
$end_pro_date       = "";
$img                = "";
$img_web            = "";
$is_active          = "";


$readonly = "";

if($action == 'EDIT'){
  $btn = 'Update changes';

  $sqls   = "SELECT * FROM data_mas_package WHERE  package_id = '$id'";

  $query      = DbQuery($sqls,null);
  $row        = json_decode($query, true);
  $rows       = $row['data'];


  // package_id
	// package_code
	// package_name
	// package_detail
	// package_type
	// num_use
	// package_unit
	// max_use
	// package_price
  // type_vat
  // notify_num
  // notify_unit
  // package_status
  // create_by
  // create_date
  // update_by
  // update_date
  // seq
  // company_code
  // branch1
  // branch2
  // branch3
  // branch4
  // branch5
  // promotion
  // start_pro_date
  // end_pro_date
  // img
  // img_web

  $package_id         = $rows[0]['package_id'];
  $package_code       = $rows[0]['package_code'];
  $package_name       = $rows[0]['package_name'];
  $package_detail     = $rows[0]['package_detail'];
  $package_type       = $rows[0]['package_type'];
  $num_use            = $rows[0]['num_use'];
  $package_unit       = $rows[0]['package_unit'];
  $max_use            = $rows[0]['max_use'];
  $package_price      = $rows[0]['package_price'];
  $type_vat           = $rows[0]['type_vat'];
  $notify_num         = $rows[0]['notify_num'];
  $notify_unit	      = $rows[0]['notify_unit'];
  $package_status     = $rows[0]['package_status'];
  $company_code       = $rows[0]['company_code'];
  $branch1            = $rows[0]['branch1'];
  $branch2            = $rows[0]['branch2'];
  $branch3            = $rows[0]['branch3'];
  $branch4            = $rows[0]['branch4'];
  $branch5            = $rows[0]['branch5'];
  $promotion          = $rows[0]['promotion'];
  $start_pro_date     = DateEn($rows[0]['start_pro_date']);
  $end_pro_date       = DateEn($rows[0]['end_pro_date']);
  $img                = $rows[0]['img'];
  $img_web            = $rows[0]['img_web'];

  $img          = "../../image/promotions/mobile/".$img;


  $readonly = "return false;";
}
else if($action == 'ADD')
{
 $btn = 'Save changes';
 if($branch_code == "GYMMK01"){
   $branch1 = "Y";
 }
 else if($branch_code == "GYMMK02")
 {
   $branch2 = "Y";
 }

}

//echo $img;
?>
<input type="hidden" name="package_id" value="<?= $package_id ?>">
<input type="hidden" name="action" value="<?= $action ?>">
<div class="modal-body">
  <div class="row">
    <div class="col-md-2">
      <div class="form-group">
        <label>Code</label>
        <input value="<?= $package_code ?>" name="package_code" id="package_code" type="text" class="form-control" placeholder="Code" required readonly data-smk-msg="&nbsp;">
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>ประเภท Package</label>
        <div class="radio">
          <label>
            <input type="radio" name="type_vat" value="PT" <?= $type_vat== "PT"?"checked":"" ?> onclick="<?=$readonly ?>genPackageCode('PT','<?=$branch_code?>')" required data-smk-msg="&nbsp;">
            Personal Trainer
          </label>
            	&nbsp;
              &nbsp;
          <label>
            <input type="radio" name="type_vat" value="MB" <?= $type_vat== "MB"?"checked":"" ?> onclick="<?=$readonly ?>genPackageCode('MB','<?=$branch_code?>')" required data-smk-msg="&nbsp;">
            Member
          </label>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>สาขา</label>
        <div class="checkbox">
          <label>
            <input type="checkbox"  name="branch1" value="Y" <?= $branch1 == "Y"?"checked":"" ?> data-smk-msg="&nbsp;">
            ราชพฤกษ์
         </label>
            &nbsp;
            &nbsp;
        <label>
            <input type="checkbox"  name="branch2" value="Y" <?= $branch2 == "Y"?"checked":"" ?> data-smk-msg="&nbsp;">
            HomePro จรัญ
        </label>
        </div>
      </div>
    </div>
  </div>
    <div class="row">
    <div class="col-md-10">
      <div class="form-group">
        <label>Promotion Name</label>
        <input value="<?= $package_name ?>" name="package_name" type="text" class="form-control" placeholder="Name" required data-smk-msg="&nbsp;">
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>ราคา</label>
        <input class="form-control" value="<?= $package_price ?>" required name="package_price" type="text" onKeyPress="return chkNumber(this)" style="text-align:right;" data-smk-msg="&nbsp;">
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>จำนวนการใช้งาน</label>
        <div>
        <input class="form-control pull-left text-right" onKeyPress="return chkNumber(this)" value="<?= $num_use ?>"  name="num_use" type="text" required style="width: 48%;margin-right:2%" data-smk-msg="&nbsp;">
        <select name="package_unit" id="package_unit" class="form-control select2  pull-left" style="width: 50%;"  required data-smk-msg="&nbsp;">
          <option value="">&nbsp;</option>
          <option value="DAYS" <?= $package_unit== "DAYS"?"selected":"" ?> >วัน</option>
          <option value="MONTHS" <?= $package_unit== "MONTHS"?"selected":"" ?>>เดือน</option>
          <option value="TIMES" <?= $package_unit== "TIMES"?"selected":"" ?>>ครั้ง</option>
        </select>
      </div>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>อายุใช้งาน (วัน)</label>
        <input class="form-control text-right" id="max_use" value="<?= $max_use ?>"  name="max_use" onKeyPress="return chkNumber(this)" type="text" required data-smk-msg="&nbsp;">
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>แจ้งเตือน(วัน/ครั้ง)</label>
        <input class="form-control text-right" id="notify_num" value="<?= $notify_num ?>"  name="notify_num" onKeyPress="return chkNumber(this)" type="text" required data-smk-msg="&nbsp;">
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>วันที่เริ่มโปรโมชั่น</label>
        <input data-smk-msg="&nbsp;" class="form-control datepicker" value="<?= $start_pro_date ?>" required name="start_pro_date" type="text" data-provide="datepicker" data-date-language="th-en" readonly style="background-color:#fff;">
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>วันที่สิ้นสุดโปรโมชั่น</label>
        <input data-smk-msg="&nbsp;" class="form-control datepicker" value="<?= $end_pro_date ?>" required name="end_pro_date" type="text" data-provide="datepicker" data-date-language="th-en" readonly style="background-color:#fff;">
      </div>
    </div>
    <div class="col-md-12">
      <div class="form-group">
        <label>Description</label>
        <textarea class="form-control" rows="5" name="package_detail" placeholder="รายละเอียด ..."><?= $package_detail ?></textarea>
      </div>
    </div>
    <div class="col-md-5">
      <div class="form-group">
        <label>Image</label>
        <input value="" name="file" type="file" onchange="readURL(this,'showImg');" class="form-control" placeholder=""  accept="image/*" >
        <input class="form-control" id="img" type="hidden" value="<?=$img?>">
        <input class="form-control" name="img" type="hidden" value="<?=$img?>">
        <input class="form-control" name="img_web" type="hidden" value="<?=$img_web?>">
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>ประเภทโปรโมชั่น</label>
        <div class="radio">
          <label>
            <input type="radio" name="promotion" value="N" <?= $promotion== "N"?"checked":"" ?> required data-smk-msg="&nbsp;">
            ลูกค้าใหม่
          </label>
            	&nbsp;
              &nbsp;
          <label>
            <input type="radio" name="promotion" value="M" <?= $promotion== "M"?"checked":"" ?> required data-smk-msg="&nbsp;">
            สมาชิก
          </label>
          &nbsp;
          &nbsp;
          <label>
            <input type="radio" name="promotion" value="A" <?= $promotion== "A"?"checked":"" ?> required data-smk-msg="&nbsp;">
            ทั้ง 2 แบบ
          </label>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>Status</label>
        <select name="package_status" class="form-control select2" style="width: 100%;" required>
          <option value="Y" <?= ($package_status == 'Y' ? 'selected="selected"':'') ?> >ใช้งาน</option>
          <option value="N" <?= ($package_status == 'N' ? 'selected="selected"':'') ?> >ไม่ใช้งาน</option>
        </select>
      </div>
    </div>
    <div class="col-md-12">
      <div class="form-group" id="showImg">
          <img src="<?= $img ?>" onerror="this.onerror='';this.src='../../image/picture.png'" style="height: 100px;">
      </div>
    </div>

  </div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
<?php if($action != "SHOW"){ ?>
<button type="submit" class="btn btn-primary btn-flat"><?=$btn?></button>
<?php } ?>
</div>
