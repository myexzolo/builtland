<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ระบบวางบิล Online | จัดการ Vendor</title>
    <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
    <?php
      include("../../inc/css-header.php");
      $_SESSION['RE_URI'] = $_SERVER['REQUEST_URI'];
    ?>
    <link rel="stylesheet" href="css/vendor.css">
  </head>
  <body class="hold-transition skin-purple-light sidebar-mini" onload="showProcessbar();showSlidebar();">
    <div class="wrapper">
      <?php include("../../inc/header.php"); ?>

      <?php include("../../inc/sidebar.php"); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>จัดการ Vendor</h1>
          <ol class="breadcrumb">
            <li><a href="../../pages/home/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Vendor</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <?php //include("../../inc/boxes.php"); ?>
          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <div class="col-md-12">

              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title"></h3>

                  <div class="box-tools" style="width:320px">
                    <?php if($_SESSION['ROLE_USER']['is_insert'])
                    {
                    ?>
                      <a class="btn btn-social btn-success btnAdd pull-right"  onclick="showForm('ADD')">
                        <i class="fa fa-plus"></i> Vendor
                      </a>
                    <?php
                    }
                    ?>
                    <?php if($_SESSION['ROLE_USER']['is_import'])
                    {
                    ?>
                      <a class="btn btn-social btn-primary btnAdd pull-right" style="margin-right:5px" onclick="upload()">
                        <i class="fa fa-sign-in"></i> นำเข้า
                      </a>
                    <?php
                    }
                    ?>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <div id="showTable"></div>
                </div>
              </div>
              <!-- /.box -->

              <!-- Modal -->
              <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="myModalLabel">Management Vendor</h4>
                    </div>
                    <form id="formAdd"  novalidate enctype="multipart/form-data" autocomplete="off">
                    <!-- <form id="formAdd" novalidate enctype="multipart/form-data" action="ajax/AEDVendors.php" method="post"> -->
                      <div id="show-form"></div>
                    </form>
                  </div>
                </div>
              </div>
              <div class="modal fade" id="myModalReset" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title">Reset Password Vendor</h4>
                    </div>
                    <form id="formResetPassword" novalidate enctype="multipart/form-data">
                    <!-- <form id="formAddModule" data-smk-icon="glyphicon-remove-sign" novalidate enctype="multipart/form-data" action="ajax/AEDVendors.php" method="post"> -->
                      <div id="show-form-rw"></div>
                    </form>
                  </div>
                </div>
              </div>
              <div class="modal fade bs-example-modal-lg" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="myModalLabel2">นำเข้าข้อมูล Vendor</h4>
                    </div>
                    <form id="formDataUpload" novalidate enctype="multipart/form-data" >
                    <!-- <form id="formDataUpload" enctype="multipart/form-data" action="ajax/manageUpload.php" method="post"> -->
                      <div class="modal-body" id="formShow2">
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal" style="width:100px;">ปิด</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>

            </div>
          </div>
          <!-- /.row -->
        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->

      <?php include("../../inc/footer.php"); ?>
    </div>
    <!-- ./wrapper -->
    <?php include("../../inc/js-footer.php"); ?>

      <script src="js/vendor.js"></script>
    </body>
  </html>
