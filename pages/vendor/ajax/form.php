<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = $_POST['value'];
$id     = isset($_POST['id'])?$_POST['id']:"";

$vendor_code          = "";
$vendor_name          = "";
$vendor_business      = "";
$vendor_address       = "";
$vendor_tel           = "";
$vendor_tax           = "";
$vendor_prosonal_id   = "";
$term                 = "0";
$user_id              = "";
$is_active            = "";

$readonly = "";

if($action == 'EDIT'){
  $btn = 'Update changes';
  $readonly = "readonly";


  $sql   = "SELECT * FROM t_vendor WHERE vendor_id = '$id'";

  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $errorInfo  = $json['errorInfo'];
  $dataCount  = $json['dataCount'];
  $row        = $json['data'];

  //echo $sql;

  $vendor_code          = $row[0]['vendor_code'];
  $vendor_name          = $row[0]['vendor_name'];
  $vendor_business      = $row[0]['vendor_business'];
  $vendor_address       = trim($row[0]['vendor_address']);
  $vendor_tel           = $row[0]['vendor_tel'];
  $vendor_tax           = $row[0]['vendor_tax'];
  $vendor_prosonal_id   = $row[0]['vendor_prosonal_id'];
  $term                 = $row[0]['term'];
  $user_id              = $row[0]['user_id'];
  $is_active            = $row[0]['is_active'];
}
if($action == 'ADD'){
 $btn = 'Save changes';
}
?>
<input type="hidden" id="action" name="action" value="<?=$action?>">
<input type="hidden" name="vendor_id" value="<?=$id?>">
<input type="hidden" name="user_id" value="<?=$user_id?>">
<div class="modal-body">
  <div class="row">
    <div class="col-md-3">
      <div class="form-group">
        <label>Vendor Code</label>
        <input value="<?=$vendor_code?>" id="vendor_code" onblur="checkVendorCode()"  data-smk-msg="&nbsp;"
        name="vendor_code" <?= $readonly ?>  type="text" class="form-control" placeholder="Code" required>
      </div>
    </div>
    <div class="col-md-9">
      <div class="form-group">
        <label>Vendor Name</label>
        <input value="<?=$vendor_name?>" name="vendor_name" type="text" class="form-control" placeholder="Name"  required data-smk-msg="&nbsp;">
      </div>
    </div>
    <?php if($action == 'ADD'){ ?>
    <div class="col-md-4">
      <div class="form-group">
        <label>Password</label>
        <input value="" name="user_password" id="pass1" type="password" autocomplete="new-password" class="form-control" placeholder="Password" required data-smk-msg="&nbsp;">
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>Confirm Password</label>
        <input value="" name="cfm_user_password" id="pass2" type="password" autocomplete="new-password" class="form-control" placeholder="Confirm Password" required data-smk-msg="&nbsp;">
      </div>
    </div>
    <?php } ?>
    <div class="col-md-12">
      <div class="form-group">
        <label>Business</label>
        <input value="<?=$vendor_business ?>" name="vendor_business" type="text" class="form-control" placeholder="Business" >
      </div>
    </div>
    <div class="col-md-12">
      <div class="form-group">
        <label>Address</label>
        <textarea  name="vendor_address" class="form-control" rows="2" placeholder="Address ..."><?=$vendor_address ?></textarea>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>Term</label>
        <input value="<?=$term ?>" name="term" type="number" class="form-control"  required  style="text-align:right;" data-smk-msg="&nbsp;">
      </div>
    </div>
    <div class="col-md-5">
      <div class="form-group">
        <label>Tel.</label>
        <input value="<?=$vendor_tel ?>" name="vendor_tel" type="text" class="form-control" >
      </div>
    </div>
    <div class="col-md-5">
      <div class="form-group">
        <label>Tax ID</label>
        <input value="<?=$vendor_tax ?>" name="vendor_tax" type="text" class="form-control" >
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>Prosonal ID.</label>
        <input value="<?=$vendor_prosonal_id ?>" name="vendor_prosonal_id" type="text" class="form-control" >
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>Status</label>
        <select name="is_active" class="form-control select2" style="width: 100%;" required>
          <option value="Y" <?=@$is_active=='Y'?"selected":""?>>ใช้งาน</option>
          <option value="N" <?=@$is_active=='N'?"selected":""?>>ไม่ใช้งาน</option>
        </select>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;">บันทึก</button>
</div>
