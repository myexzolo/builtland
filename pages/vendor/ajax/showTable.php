<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<style>
  th {
    text-align: center;
    background-color: #ebebeb;
  }
</style>
<table class="table table-bordered table-striped table-hover" id="tableDisplay" style="min-width:1300px;width:100%">
  <thead>
    <tr class="text-center">
      <th style="width:40px;">No.</th>
      <th style="width:30px;">User</th>
      <th >ชื่อ</th>
      <th >Term (วัน)</th>
      <th >เบอร์โทร</th>
      <th >Tax ID</th>
      <th >Prosonal ID</th>
      <th >สถานะ</th>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <th style="width:100px;">Re Password</th>
      <th style="width:50px;">Edit</th>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
      <th style="width:50px;">Del</th>
      <?php
      }
      ?>
    </tr>
  </thead>
  <tbody>
    <?php
      //echo $str;
      $sqls   = "SELECT *
                 FROM t_vendor
                 where 	is_active != 'D'
                 ORDER BY update_date DESC";

      $querys     = DbQuery($sqls,null);
      $json       = json_decode($querys, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];

      if($dataCount > 0)
      {
      foreach ($rows as $key => $value) {
        $vendor_code = $value['vendor_code'];
    ?>
    <tr class="text-center">
      <td><?=$key+1;?></td>
      <td align="left"><?=$vendor_code?></td>
      <td align="left"><?=$value['vendor_name'];?></td>
      <td align="right"><?=$value['term'];?></td>
      <td align="left"><?=$value['vendor_tel'];?></td>
      <td align="left"><?=$value['vendor_tax'];?></td>
      <td align="left"><?=$value['vendor_prosonal_id'];?></td>
      <td><?=$value['is_active']=='Y'?"ใช้งาน":"ไม่ใช้งาน";?></td>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <td>
        <a class="btn_point text-yellow"><i class="fa fa-key" onclick="resetPass('<?=$value['vendor_id']?>')"></i></a>
      </td>
      <td>
        <a class="btn_point"><i class="fa fa-edit" onclick="showForm('EDIT','<?=$value['vendor_id']?>')"></i></a>
      </td>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
      <td>
        <a class="btn_point text-red"><i class="fa fa-trash-o" onclick="del('<?=$value['vendor_id']?>','<?=$vendor_code?>')"></i></a>
      </td>
      <?php
      }
      ?>
    </tr>
  <?php } } ?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false,
      'oLanguage': {
        'sEmptyTable': 'ไม่พบข้อมูล'
      }
    }).columns.adjust();
  })
</script>
