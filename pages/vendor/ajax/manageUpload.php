<?php
require_once '../../../Classes/PHPExcel.php';
include '../../../Classes/PHPExcel/IOFactory.php';
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');


$branchCode     = $_SESSION['branchCode'];
$name           = "vendor_".date("His");
$target_file    = "upload/".$name.".xlsx";
$inputFileName  = $target_file;
$file_name      = $_FILES["filepath"]["name"];

if (file_exists($target_file)) {
  unlink($target_file);
}

$success = 0;
$fail    = 0;
$total   = 0;
$fail_list = "";

if (move_uploaded_file($_FILES["filepath"]["tmp_name"], $target_file)) {
   try
   {

     $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
     $objReader = PHPExcel_IOFactory::createReader($inputFileType);
     $objReader->setReadDataOnly(true);
     $objPHPExcel = $objReader->load($inputFileName);

     $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
     $highestRow = $objWorksheet->getHighestRow();
     $highestColumn = $objWorksheet->getHighestColumn();

     //$headingsArray = $objWorksheet->rangeToArray('A7:'.$highestColumn.'1',null, true, true, true);
     //$headingsArray = $headingsArray[1];

     //$headerName = $headingsArray['A'];

     $headingsArray = array("A" => "NO",
                            "B" => "entry_date",
                            "C" => "import",
                            "F" => "vendor_code",
                            "G" => "vendor_name",
                            "J" => "vendor_business",
                            "K" => "term",
                            "M" => "vendor_address",
                            "O" => "vendor_tel",
                            "P" => "vendor_tax",
                            "S" => "vendor_prosonal_id",
                          );
     $r = -1;

     $namedDataArray = array();
     for ($row = 8; $row <= $highestRow; ++$row) {
         $dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
         if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
             ++$r;
             foreach($headingsArray as $columnKey => $columnHeading) {
                 $namedDataArray[$r][$columnHeading] = $dataRow[$row][$columnKey];
             }
         }
     }
     $i = 0;
       foreach ($namedDataArray as $result) {
          try
          {
            $i++;

            $NO                 = trim($result["NO"]);
            $vendor_code        = trim($result["vendor_code"]);
            $vendor_name        = trim($result["vendor_name"]);
            $vendor_business    = trim($result["vendor_business"]);
            $vendor_address     = $result["vendor_address"];
            //$user_password  = @md5(isset($result["user_password"])?$result["user_password"]:"");
            $vendor_tel         = $result["vendor_tel"];
            $vendor_tax         = $result["vendor_tax"];
            $vendor_prosonal_id = $result["vendor_prosonal_id"];
            $term               = $result["term"] == ""?'0':$result["term"];



            if(!is_numeric($NO))
            {
              continue;
            }

            $total++;

            if($vendor_code == "" || $vendor_name == "")
            {
              $fail++;
              if($fail_list == ""){
                $fail_list = $NO;
              }else{
                $fail_list .= ", ".$NO;
              }
              continue;
            }

            $sql = "SELECT * FROM t_user WHERE user_login = '$vendor_code'";
            $json = DbQuery($sql,null);

            $obj = json_decode($json, true);
            $dataCount = $obj['dataCount'];
            $strSQL = "";
            $typeAction = "";
            if($dataCount > 0){
              $typeAction = "update";
              $strSQL = "UPDATE t_user SET
                          user_name      = '$vendor_name',
                          update_date    = NOW(),
                          user_id_update = '1'
                          WHERE user_login = '$vendor_code'";
            }else{
              $typeAction = "insert";
              $user_password = @md5($vendor_code);
              $strSQL = "INSERT INTO t_user (
                        user_login,user_password,
                        user_name,user_last,
                        is_active,role_list,
                        update_date,branch_code,
                        user_id_update,department_id)
                        VALUES(
                       '$vendor_code','$user_password',
                       '$vendor_name','',
                       'Y','999',
                       NOW(),'',
                       '1','0')";
            }
            // echo $strSQL."<br>";
            $js      = DbQuery($strSQL,null);
            $row     = json_decode($js, true);
            $status  = $row['status'];
            $user_id = $row['id'];
            //print_r($row['errorInfo'])."<br>";
            $errorInfo  = isset($row['errorInfo'][0])?$row['errorInfo'][0]:1;
            if($errorInfo != "00000"){
              $fail++;
              if($fail_list == ""){
                $fail_list = $NO;
              }else{
                $fail_list .= ", ".$NO;
              }
              continue;
            }else{
              if($typeAction == "update"){
                $strVendor = "UPDATE t_vendor SET
                              vendor_name      = '$vendor_name',
                              vendor_business  = '$vendor_business',
                              vendor_address   = '$vendor_address',
                              vendor_tel       = '$vendor_tel',
                              vendor_tax       = '$vendor_tax',
                              vendor_prosonal_id = '$vendor_prosonal_id',
                              term = '$term',
                              user_id_update = '1'
                              WHERE vendor_code = '$vendor_code'";
              }else if($typeAction == "insert"){
                $strVendor = "INSERT INTO t_vendor (
                              vendor_code,vendor_name,
                              vendor_business,vendor_address,
                              vendor_tel,vendor_tax,
                              vendor_prosonal_id,term,
                              user_id,is_active,create_by,create_date)
                              VALUES(
                                '$vendor_code','$vendor_name',
                                '$vendor_business','$vendor_address',
                                '$vendor_tel','$vendor_tax',
                                '$vendor_prosonal_id','$term',
                                '$user_id','Y','1',NOW())";
              }
              DbQuery($strVendor,null);
              $success++;
            }
          }catch (Exception $ex) {
            $fail++;
          }
        }
   } catch (Exception $e) {
     header('Content-Type: application/json');
     exit(json_encode(array('status' => 'danger', 'message' => 'ไม่พบไฟล์หรือรูปแบบไฟล์ไม่ถูกต้อง', 'success'=>$success, 'fail'=>$fail, 'total'=>$total)));
   }

   if($total == $success){
     $status_upload = "S";
   }else{
     $status_upload = "F";
   }

    $str = "INSERT INTO t_history_upload
           (file_name,status_upload,total_record,success_record,fail_record,fail_list, path_file,type_file,branch_code)
           VALUES
           ('$file_name','$status_upload','$total','$success','$fail','$fail_list','$target_file','vender','$branchCode')";

   $query    = DbQuery($str,null);
   $json     = json_decode($query, true);
   $status   = $json['status'];

   if($status == "success"){
     header('Content-Type: application/json');
     exit(json_encode(array('status' => 'success', 'message' => 'บันทึกสมบูรณ์', 'success'=>$success, 'fail'=>$fail, 'total'=>$total )));
   }else{
     header('Content-Type: application/json');
     exit(json_encode(array('status' => 'danger', 'message' => 'บันทึกไม่สมบูรณ์', 'success'=>$success, 'fail'=>$fail, 'total'=>$total)));
   }
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger', 'message' => 'ไม่พบไฟล์หรือรูปแบบไฟล์ไม่ถูกต้อง', 'success'=>$success, 'fail'=>$fail, 'total'=>$total)));
}


?>
