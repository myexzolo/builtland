<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$id = $_POST['id'];


  $sql   = "SELECT * FROM t_vendor WHERE vendor_id = '$id'";

  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $errorInfo  = $json['errorInfo'];
  $dataCount  = $json['dataCount'];
  $row        = $json['data'];

  $vendor_code          = $row[0]['vendor_code'];
  $vendor_name          = $row[0]['vendor_name'];
  $vendor_business      = $row[0]['vendor_business'];
  $vendor_address       = trim($row[0]['vendor_address']);
  $vendor_tel           = $row[0]['vendor_tel'];
  $vendor_tax           = $row[0]['vendor_tax'];
  $vendor_prosonal_id   = $row[0]['vendor_prosonal_id'];
  $term                 = $row[0]['term'];
  $user_id              = $row[0]['user_id'];
  $is_active            = $row[0]['is_active'];

?>
<input type="hidden" id="action" name="action" value="RESET">
<input type="hidden" name="user_id" value="<?=$user_id?>">
<div class="modal-body">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group">
        <label>ชื่อ</label>
        <input value="<?=@$vendor_name?>" type="text" class="form-control" placeholder="Name" required readonly>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>User Login</label>
        <input value="<?=@$vendor_code?>" type="text" class="form-control" placeholder="User Login" required readonly>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>Reset Password</label>
        <input value="" name="user_password" id="pass1" type="password" autocomplete="new-password"  class="form-control" placeholder="Password" required>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>Confirm Password</label>
        <input value="" name="cfm_user_password" id="pass2" type="password" autocomplete="new-password"  class="form-control" placeholder="Confirm Password" required>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;">บันทึก</button>
</div>
